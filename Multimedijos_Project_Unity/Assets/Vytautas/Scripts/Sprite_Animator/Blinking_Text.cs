﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blinking_Text : MonoBehaviour
{
    //public
    public float speed = 1f;
    public float bottonValue = 0.5f;
    public float upperValue = 1f;

    //private
    private Text text;

    void Start ()
    {
        text = GetComponent<Text>();
    }

	void Update ()
    {
        text.color = Color.Lerp(new Color(1, 1, 1, bottonValue), new Color(1, 1, 1, upperValue), Mathf.PingPong(Time.time * speed, 1));
    }
}
