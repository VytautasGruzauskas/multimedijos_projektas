﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogDetection : MonoBehaviour
{
    //public
    public Material blackOutMaterial;
    public Collider fogCollider;
    public float fadeSpeed = 0.09f;

    //private
    private bool isInTheFog = false;

    void Start()
    {
        Color color = blackOutMaterial.color;
        color.a = 0f;
        blackOutMaterial.color = color;
    }

    void OnApplicationQuit()
    {
        Color color = blackOutMaterial.color;
        color.a = 0f;
        blackOutMaterial.color = color;
    }



    void Update ()
    {
        Color color = blackOutMaterial.color;

        if (isInTheFog)
        {
            if(color.a < 1)
            {
                color.a += 1f * Time.deltaTime * fadeSpeed;
                blackOutMaterial.color = color;
                
            }
            else if(color.a >= 1)
            {
                Debug.Log("DEAD");
            }
        }
        else
        {
            if (color.a > 0)
            {
                color.a -= 1f * Time.deltaTime * fadeSpeed;
                blackOutMaterial.color = color;
            }
        }
	}

    private void OnTriggerExit(Collider other)
    {
        if(other == fogCollider)
            isInTheFog = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other == fogCollider)
            isInTheFog = false;
    }

}
