﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AddedControls : MonoBehaviour
{
    //public
    public Transform mainCamera;
    public float reachDistance = 3;
    public LayerMask InteractableLayers;
    public float distance = 3;
    public float rate = 10;

    //private
    private bool wantToGrab;
    private GameObject grabbedObject;
    private bool canGrab = true;
    //private CharacterController player;

    void Start ()
    {
        wantToGrab = false;
        grabbedObject = null;
        //player = mainCamera.GetComponentInParent<CharacterController>();
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.N))
        {
            if (PlayerPrefs.GetInt("Next_Scene") > 5)
            {
                PlayerPrefs.SetInt("Next_Scene", 2);
                PlayerPrefs.SetInt("Continue_Level", 2);
            }
            else
            {
                int level = PlayerPrefs.GetInt("Next_Scene") + 1;
                PlayerPrefs.SetInt("Next_Scene", level);
                PlayerPrefs.SetInt("Continue_Level", level);
            }


            Debug.Log("kita Scena ID: " + PlayerPrefs.GetInt("Next_Scene"));
            SceneManager.LoadScene("Loading_Scene");

        }


        if (Input.GetMouseButton(0))
        {
            wantToGrab = true;
        }
        else
        { 
            wantToGrab = false;
        }


        if (wantToGrab && grabbedObject == null && canGrab)
        {
            Ray ray = new Ray(mainCamera.position, mainCamera.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, reachDistance, InteractableLayers))
            {
                if (hit.collider.tag == "pickableObject")
                {
                    grabbedObject = hit.collider.gameObject;
                }

                //Debug.Log("interactinam su objektu: " + hit.collider.name);
            }
        }


        if(grabbedObject != null)
        {
            //jei laidas idetas tai ji islaisviname
            Cable cable = grabbedObject.GetComponentInParent<Cable>();
            if (cable != null)
            {
                for (int i = 0; i < cable.numberOfCableEnds; i++)
                { 
                    if (cable.cableEnds[i] == grabbedObject && cable.cableEndsPos[i] != -1)
                    {
                        if (cable.isCableAttachedToHolder(cable.cableEnds[i]))
                        {
                            cable.TakeCableFromHolder(cable.cableEnds[i]);
                        }
                    }
                }
            
            }

            Rigidbody rb = grabbedObject.GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            rb.velocity = ((mainCamera.transform.position + (mainCamera.transform.forward * distance)) - grabbedObject.transform.position) * rate;
            rb.constraints = RigidbodyConstraints.None;
            if(Vector3.Distance(mainCamera.transform.position, grabbedObject.transform.position) > 4.1f)
            {
                Debug.Log("numetam objekta");
                grabbedObject = null;
            }
        }

        if(!wantToGrab && grabbedObject != null)
        {
            grabbedObject = null;
            
        }
	}

    public void nullGrabbedObject()
    {
        //Debug.Log("nulinam");
        grabbedObject = null;
        StartCoroutine(WaitForCable());
    }
    
    IEnumerator WaitForCable()
    {
        canGrab = false;
        yield return new WaitForSecondsRealtime(1f);
        canGrab = true;
    }
}
