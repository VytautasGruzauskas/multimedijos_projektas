﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesOff : MonoBehaviour
{
    public ParticleSystem[] particles;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller;

    private void Start()
    {
        StartCoroutine(ParticleGone());
    }

    IEnumerator ParticleGone()
    {
        yield return new WaitForSeconds(3);
        controller.enabled = true;
        foreach (ParticleSystem part in particles)
        {
            part.gameObject.SetActive(false);
        }
    }
}
