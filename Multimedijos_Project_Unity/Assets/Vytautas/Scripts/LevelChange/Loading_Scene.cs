﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading_Scene : MonoBehaviour
{
	void Start ()
    {
        StartCoroutine(LoadNewScene());
	}

    IEnumerator LoadNewScene()
    {
        yield return new WaitForSeconds(1);
        AsyncOperation async = SceneManager.LoadSceneAsync(PlayerPrefs.GetInt("Next_Scene"));

        while (!async.isDone)
        {
            yield return null;
        }
    }
}
