﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChange : MonoBehaviour
{
    public GameObject playerObject;
    public PuzzleDone puzzleDone;
    public GameObject portalParticles;

    bool portalActive = false;

    void FixedUpdate()
    {
        if (puzzleDone.CheckIfPuzzlesAreSolved())
        {
            portalParticles.SetActive(true);
            portalActive = true;
        }
        else
        {
            portalParticles.SetActive(false);
            portalActive = false;
        }
    }

    //private
    private bool loadScene = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other == playerObject.GetComponent<Collider>())
        {
            if (!loadScene && portalActive)
            {
                loadScene = true;

                switch (puzzleDone.level)//domo scenos eile: 6
                {
                    case PuzzleDone.CurrentLevel.Level2://luko scena. eile: 2
                        PlayerPrefs.SetInt("Next_Scene", 3);
                        PlayerPrefs.SetInt("Continue_Level", 3);
                        break;

                    case PuzzleDone.CurrentLevel.Level1://viltes scena. eile: 3
                        PlayerPrefs.SetInt("Next_Scene", 4);
                        PlayerPrefs.SetInt("Continue_Level", 4);
                        break;

                    case PuzzleDone.CurrentLevel.Level4://laidu scena. eile: 4
                        PlayerPrefs.SetInt("Next_Scene", 5);
                        PlayerPrefs.SetInt("Continue_Level", 5);
                        break;

                    case PuzzleDone.CurrentLevel.Level3://desert scena. eile: 5
                        PlayerPrefs.SetInt("Next_Scene", 6);
                        PlayerPrefs.SetInt("Continue_Level", 6);
                        break;

                }
                Debug.Log("kita Scena ID: "+PlayerPrefs.GetInt("Next_Scene"));
                SceneManager.LoadScene("Loading_Scene");
            }
        }
    }
}
