﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableHolderAttachTrigger : MonoBehaviour
{
    public int posNumber;

    private CableHolder holder;

    private void Start()
    {
        holder = GetComponentInParent<CableHolder>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.name == "kabelio_dalis_Pradzia")
            holder.OnMyTriggerEnter(other, posNumber, 0);
        else if (other.name == "kabelio_dalis_Pabaiga")
            holder.OnMyTriggerEnter(other, posNumber, 1);
    }

}
