﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleDone : MonoBehaviour
{
    //public skripto
    public enum CurrentLevel {Level1, Level2, Level3, Level4};
    public CurrentLevel level = CurrentLevel.Level1;



    //level2 Lukas 
    //public
    public GameObject puzzleFinished;
    public float smoothtime = 0.3F;
    public float height;
    public float bottomHeigth = -10.85f;
    //private
    private Vector3 velocity = Vector3.zero;
    private Quaternion startRot;

    //private skripto
    private bool isPuzzleSolved = false;

    public bool CheckIfPuzzlesAreSolved()
    {
        return isPuzzleSolved;
    }

    public void PuzzleSolved()
    {
        if (!isPuzzleSolved)
        {
            Debug.Log("Uzduotis padaryta!");
            isPuzzleSolved = true;
        }
    }

    private void Start()
    {
        switch (level)
        {
            case CurrentLevel.Level3:
                {
                    startRot = Quaternion.Euler(0, -90, 0);

                    break;
                }
        }
    }

    void Update()
    {
        if (isPuzzleSolved)
        {
            switch (level)
            {
                case CurrentLevel.Level2:
                    {
                        Vector3 targetPosition = new Vector3(puzzleFinished.transform.position.x, height, puzzleFinished.transform.position.z);
                        puzzleFinished.transform.position = Vector3.SmoothDamp(puzzleFinished.transform.position, targetPosition,
                            ref velocity, smoothtime);
                        break;
                    }


                case CurrentLevel.Level3:
                    {

                        puzzleFinished.transform.rotation = Quaternion.Slerp(puzzleFinished.transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * smoothtime);
                       // Vector3 targetPosition = new Vector3(puzzleFinished.transform.position.x, height, puzzleFinished.transform.position.z);
                        //puzzleFinished.transform.position = Vector3.SmoothDamp(puzzleFinished.transform.position, targetPosition,
                        //    ref velocity, smoothtime);
                        break;
                    }
            }

            
        }

        if (!isPuzzleSolved)
        {
            switch(level)
            {
                case CurrentLevel.Level2:
                    {
                        Vector3 targetPosition = new Vector3(puzzleFinished.transform.position.x, bottomHeigth, puzzleFinished.transform.position.z);
                        puzzleFinished.transform.position = Vector3.SmoothDamp(puzzleFinished.transform.position, targetPosition,
                            ref velocity, smoothtime);
                        break;
                    }

                case CurrentLevel.Level3:
                    {

                        puzzleFinished.transform.rotation = Quaternion.Slerp(puzzleFinished.transform.rotation, startRot, Time.deltaTime * smoothtime);
                        // Vector3 targetPosition = new Vector3(puzzleFinished.transform.position.x, height, puzzleFinished.transform.position.z);
                        //puzzleFinished.transform.position = Vector3.SmoothDamp(puzzleFinished.transform.position, targetPosition,
                        //    ref velocity, smoothtime);
                        break;
                    }
            }
        }
    }

    public void PuzzleStillNotSolved()
    {
       // Debug.Log("Uzduotis dar daroma!");
    }

    public void PuzzleUnSolved()
    {
        if(isPuzzleSolved)
        {
            isPuzzleSolved = false;
            Debug.Log("Uzduotis vel nepadaryta");
        }
    }
}
