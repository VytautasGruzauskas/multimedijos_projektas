﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPress : MonoBehaviour
{
    //public
    public LayerMask interactableLayers;
    public bool ArReikia_useKeyHint = true;
    public GameObject useKeyHint;

    //private
    private float reach;
    private Transform mainCamera;

    void Start()
    {
        reach = GameObject.Find("FirstPersonCharacter").GetComponent<AddedControls>().reachDistance - 1;
        mainCamera = GameObject.Find("FirstPersonCharacter").GetComponent<AddedControls>().mainCamera;
        if (ArReikia_useKeyHint)
            useKeyHint.SetActive(false);
    }

    void Update()
    {
        Ray ray = new Ray(mainCamera.position, mainCamera.forward);
        RaycastHit hit;
        if (ArReikia_useKeyHint)
        {
            if (useKeyHint.activeInHierarchy)
                useKeyHint.SetActive(false);

            if (Physics.Raycast(ray, out hit, reach, interactableLayers))
            {
                if (hit.collider.tag == "pickableObject")
                {
                    if (hit.collider.gameObject.transform.GetChild(0).tag == "Planet")
                    {
                        if (hit.collider.gameObject.GetComponent<Rigidbody>().isKinematic && !useKeyHint.activeInHierarchy)
                            useKeyHint.SetActive(true);
                        else if (!hit.collider.gameObject.GetComponent<Rigidbody>().isKinematic && useKeyHint.activeInHierarchy)
                            useKeyHint.SetActive(false);
                    }
                }
                else if (hit.collider.tag == "Button")
                {
                    if (!useKeyHint.activeInHierarchy)
                        useKeyHint.SetActive(true);
                    else if (useKeyHint.activeInHierarchy)
                        useKeyHint.SetActive(false);
                }
                else if (hit.collider.tag == "teleskopas")
                {
                    if (!useKeyHint.activeInHierarchy)
                        useKeyHint.SetActive(true);
                    else if (useKeyHint.activeInHierarchy)
                        useKeyHint.SetActive(false);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            ray = new Ray(mainCamera.position, mainCamera.forward);

            if (ArReikia_useKeyHint)
                if (useKeyHint.activeInHierarchy)
                    useKeyHint.SetActive(false);

            if (Physics.Raycast(ray, out hit, reach, interactableLayers))
            {
                if (hit.collider.tag == "Button")
                {
                    hit.collider.GetComponent<Button>().OnPressed();
                }

                if(hit.collider.tag == "pickableObject")
                {
                    if (hit.collider.gameObject.transform.GetChild(0).tag == "Planet")
                    {
                        hit.collider.GetComponent<Button>().OnPressedPlanet(hit.collider.gameObject);
                    }
                }




            }
        }
    }
}
