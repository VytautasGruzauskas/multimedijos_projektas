﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CableHolder : MonoBehaviour
{
    //public
    public bool ElectricitySource;
    public bool ElectricityDestination;
    public bool ElectricityRunning;

    public int numberOfCables;

    public Cable[] attachedCables;

    public Material offMaterial;
    public Material onMaterial;
    public GameObject materialChangeObject;
    public Light holderLight;


    public CableList cableList;

    void Start ()
    {
        attachedCables = new Cable[numberOfCables];

        if (ElectricitySource)
            ElectricityRunning = true;
        else
            ElectricityRunning = false;

        //cableList = FindObjectOfType<CableList>();

        ChangeHolderMaterial();
    }

    public bool isCableEndAttaced(GameObject cableEnd)
    {
        for (int i = 0; i < attachedCables.Length; i++)
            if(attachedCables[i] != null)
                for (int j = 0; j < attachedCables[i].numberOfCableEnds; j++)
                    if (attachedCables[i].cableEnds[j].name == cableEnd.name)
                        return true;
        return false;
    }

    public void ReCheckAttachedCables()
    {
        bool surado = false;
        for (int i = 0; i < attachedCables.Length; i++)
        {
            if (attachedCables[i] != null)
            {
                surado = false;
                for (int j = 0; j < attachedCables[i].numberOfCableEnds; j++)
                {
                    if(attachedCables[i].attachedHolder[j] == this)
                    {
                        surado = true;
                    }
                }
                if(!surado)
                {
                    attachedCables[i] = null;
                }
            }
        }
    }

    public void RemoveCable(int pos)
    {
        attachedCables[pos] = null;

        cableList.RecheckHolders();
    }

    public bool hasCablesAttached()
    {
        for (int i = 0; i < attachedCables.Length; i++)
        {
            if (attachedCables[i] != null) return true;
        }
        return false;
    }

    public void CheckIfElectricityDelivered()
    {
        if(ElectricityDestination && ElectricityRunning)
        {
            cableList.puzzle.puzzleCompleted = true;
            cableList.puzzle.CheckIfCompleted();
        }
        else if(ElectricityDestination)
        {

            cableList.puzzle.puzzleCompleted = false;
            cableList.puzzle.CheckIfCompleted();
        }
    }

    public void ChangeHolderMaterial()
    {
        Material[] CurMaterial = materialChangeObject.transform.GetComponent<Renderer>().materials;

        if (ElectricityRunning)
        {
            CurMaterial[0] = onMaterial;
            holderLight.gameObject.SetActive(true);
        }
        else
        {
            CurMaterial[0] = offMaterial;
            holderLight.gameObject.SetActive(false);
        }
        materialChangeObject.transform.GetComponent<Renderer>().materials = CurMaterial;
    }

    public void OnMyTriggerEnter(Collider other, int index, int cableIndex)
    {
        if (attachedCables[index] == null)
        {
            Cable cable = other.transform.parent.gameObject.GetComponent<Cable>();

            if (cable.cableEndsPos[cableIndex] == -1)
            {
                attachedCables[index] = cable;
                cable.PlaceCableInHolder(gameObject, other.gameObject, index, cableIndex);
                FindObjectOfType<AddedControls>().nullGrabbedObject();
                //Debug.Log("Uzdejai kabeli i tinkama vieta");
            }
        }
    }

}

