﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableList : MonoBehaviour
{
    //public
    public List<CableHolder> CableHolders;
    public Puzzle puzzle;

    void Start()
    {
        CableHolders = new List<CableHolder>();
    }

    public int AddHolderToList(CableHolder holder)
    {

        if (CableHolders.Count == 0)
        {
            if (holder.ElectricitySource)
            {
                CableHolders.Add(holder);
            }
        }

        foreach (CableHolder cycleHolder in CableHolders)
        {
            if (AreHoldersConnected(cycleHolder, holder))
            {
                if (!CableHolders.Contains(holder))
                {
                    CableHolders.Add(holder);
                    break;
                }
            }
        }


        CableHolder[] allCableHolders = FindObjectsOfType<CableHolder>();

        gohere:

        for (int i = 0; i < allCableHolders.Length; i++)
        {
            if (!CableHolders.Contains(allCableHolders[i]))
            {
                foreach (CableHolder cycleHolder in CableHolders)
                {
                    if (AreHoldersConnected(cycleHolder, allCableHolders[i]))
                    {
                        CableHolders.Add(allCableHolders[i]);
                        goto gohere;
                    }
                }
            }

        }

        UpdateElectricity();

        return 1;
    }

    public void RecheckHolders()
    {
        //CableHolder[] allCableHolders = FindObjectsOfType<CableHolder>();
        //List<CableHolder> tikrinti = new List<CableHolder>();
        /*gohere:
        for (int i = 0; i < allCableHolders.Length; i++)
        {
            if (CableHolders.Contains(allCableHolders[i]))
            {
                bool susijunge = false;
                //CableHolder hold = null;
                foreach (CableHolder cycleHolder in CableHolders)
                {
                    if (cycleHolder != allCableHolders[i])
                    {
                        if (AreHoldersConnected(cycleHolder, allCableHolders[i]) && !tikrinti.Contains(allCableHolders[i]))
                        {
                            susijunge = true;
                            tikrinti.Add(allCableHolders[i]);
                        }
                    }
                }

                if(!susijunge)
                {
                    if (allCableHolders[i].ElectricitySource)
                    {
                        if (!allCableHolders[i].hasCablesAttached())
                        {
                            CableHolders.Clear();
                            UpdateElectricity();
                            return;
                        }

                        //break;
                    }
                    else
                    {
                        CableHolders.Remove(allCableHolders[i]);
                        //goto gohere;

                        continue;
                    }
                }


            }
        }*/
        CableHolders.Clear();

        CableHolder[] allCableHolders = FindObjectsOfType<CableHolder>();

        for (int i = 0; i < allCableHolders.Length; i++)
        {
            if (allCableHolders[i].ElectricitySource && allCableHolders[i].hasCablesAttached())
            {
                CableHolders.Add(allCableHolders[i]);
                break;
            }
        }

        

        gohere:
        for (int i = 0; i < allCableHolders.Length; i++)
        {
            if (!CableHolders.Contains(allCableHolders[i]))
            {
                foreach (CableHolder cycleHolder in CableHolders)
                {
                    if (AreHoldersConnected(cycleHolder, allCableHolders[i]))
                    {
                        CableHolders.Add(allCableHolders[i]);
                        goto gohere;
                    }
                }
            }

        }

        UpdateElectricity();
    }



    public void UpdateElectricity()
    {
        CableHolder[] allCableHolders = FindObjectsOfType<CableHolder>();
        for (int i = 0; i < allCableHolders.Length; i++)
        {
            if (CableHolders.Contains(allCableHolders[i]))
            {
                allCableHolders[i].ElectricityRunning = true;
            }
            else
            {
                if(!allCableHolders[i].ElectricitySource)
                    allCableHolders[i].ElectricityRunning = false;
            }

            if(allCableHolders[i].ElectricityDestination)
                allCableHolders[i].CheckIfElectricityDelivered();

            allCableHolders[i].ChangeHolderMaterial();
        }

    }


    bool AreHoldersConnected(CableHolder holder, CableHolder holder2)
    {
        if (holder2 == holder) return false;
        for (int i = 0; i < holder.attachedCables.Length; i++)
        {
            for (int j = 0; j < holder2.attachedCables.Length; j++)
            {
                if (holder.attachedCables[i] != null && holder2.attachedCables[j] != null)
                {
                    if (holder.attachedCables[i] == holder2.attachedCables[j])
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    void PrintListus()
    {
        string tekstas = "";

        foreach (CableHolder holder in CableHolders)
        {
            tekstas = tekstas+holder.name+";;";
        }
        tekstas = tekstas+"\n";

        Debug.Log(tekstas);
    }
}
