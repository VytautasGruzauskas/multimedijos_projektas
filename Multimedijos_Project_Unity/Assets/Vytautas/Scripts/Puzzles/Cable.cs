﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cable : MonoBehaviour
{
    //public
    //public Material OffMaterial;
    //public Material OnMaterial;

    public int numberOfCableEnds;
    public GameObject[] cableEnds;
    public int[] cableEndsPos;
    public CableHolder[] attachedHolder;

    //private
    private CableList cableList;


    private void Start()
    {
        attachedHolder = new CableHolder[numberOfCableEnds];
        cableEndsPos = new int[numberOfCableEnds];
        for (int i = 0; i < numberOfCableEnds; i++)
        {
            cableEndsPos[i] = -1;
            attachedHolder[i] = null;
        }

        cableList = FindObjectOfType<CableList>();
    }

    public void PlaceCableInHolder(GameObject holder, GameObject cableEnd, int pos, int cableIndex)
    {
        if (attachedHolder[cableIndex] == null)
        {
            attachedHolder[cableIndex] = holder.GetComponent<CableHolder>();
            Transform holderPosition;
            if (pos == 0)
                holderPosition = holder.transform.Find("Laido_Pozicija1");
            else
                holderPosition = holder.transform.Find("Laido_Pozicija2");

            if (cableEnd.name == "kabelio_dalis_Pradzia")
            {
                cableEnd.transform.rotation = Quaternion.Euler(holderPosition.eulerAngles.x ,
                    holderPosition.eulerAngles.y, 180 + holderPosition.eulerAngles.z);
            }
            else
            {
                cableEnd.transform.rotation = Quaternion.Euler( holderPosition.eulerAngles.x,
                    holderPosition.eulerAngles.y, holderPosition.eulerAngles.z);
            }

            cableEnd.transform.position = new Vector3(holderPosition.position.x, holderPosition.position.y, holderPosition.position.z);
            cableEnd.GetComponent<Rigidbody>().isKinematic = true;

            cableEndsPos[cableIndex] = pos;

            cableList.AddHolderToList(attachedHolder[cableIndex]);
        }
        else
        {
            Debug.LogWarning("nesusikalba laidai su holder, naikiname holder turima kintamaji");
            holder.GetComponent<CableHolder>().attachedCables[pos] = null;
        }
    }

    public void ChangeCableColor()
    {/*
        Material[] CurMaterial = transform.GetComponent<Renderer>().materials;
        if (attachedHolder != null)
        {
            if (attachedHolder.ElectricityRunning)
            {
                CurMaterial[2] = OnMaterial;
                transform.GetComponent<Renderer>().materials = CurMaterial;
            }
            else
            {
                CurMaterial[2] = OffMaterial;
                transform.GetComponent<Renderer>().materials = CurMaterial;
            }
        }
        else
        {
            CurMaterial[2] = OffMaterial;
            transform.GetComponent<Renderer>().materials = CurMaterial;
        }*/
    }

    public bool isCableAttachedToHolder(GameObject cableEnd)
    {
        for (int i = 0; i < numberOfCableEnds; i++)
        {
            if(cableEnds[i] == cableEnd)
            {
                if (attachedHolder[i] != null) return true;
            }
        }
        return false;
    }

    public void TakeCableFromHolder(GameObject cableEnd)
    {
        for (int i = 0; i < numberOfCableEnds; i++)
        {
            if (attachedHolder[i] != null)
            {
                if(cableEnds[i] == cableEnd)
                {
                    attachedHolder[i].RemoveCable(cableEndsPos[i]);
                    attachedHolder[i] = null;
                    cableEnd.GetComponent<Rigidbody>().isKinematic = false;
                    cableEndsPos[i] = -1;
                    break;
                }
            }
        }
    }
}
