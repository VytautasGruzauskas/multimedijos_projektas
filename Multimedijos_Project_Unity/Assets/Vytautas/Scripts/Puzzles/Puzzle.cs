﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    /*
     * norint kad public kintamasis atsirastu editoriuje reikia
     * keisti 'CustomInspector.cs' skripta esanti Assets/Editor folderi
    */

    //public
    public enum puzzleTypes { pressurePlateSimple, lightGame, pressurePlateWeight, ElectricityCables, pressurePlateParticle };
    public bool puzzleCompleted;
    public PuzzleTrigger puzzleTrigger;

    public puzzleTypes type = puzzleTypes.pressurePlateSimple;
    public Object puzzleSolver;
    public Object[] lightObjects;
    public Object[] switchObject;

    //pressurePlateWeight
    public int correctWeight;
    public Object[] CubeArray;
    public Material nonEmissiveMaterial;
    public Material emissiveMaterial;

    //ElectricityCables
    //naudojame tik paskutini Cable Holder nes tik jis svarbus del puzzle issprendimo
    //public GameObject DestinationCableHolder;

    //private
    private Light[] lights;
    private bool[] onLights;
    private GameObject[] switches;
    private float weight;
    

    void Start ()
    {
        puzzleCompleted = false;
        PreparePuzzle(type);
    } 

    public void CheckIfCompleted()
    {
        puzzleTrigger.CheckIfPuzzlesAreDone();
    }

    void PreparePuzzle(puzzleTypes type)
    {
        switch(type)
        {
            case puzzleTypes.pressurePlateSimple:
            {
                break;
            }

            case puzzleTypes.lightGame:
            {
                int i = 0;
                lights = new Light[lightObjects.Length];
                onLights = new bool[lightObjects.Length];
                switches = new GameObject[switchObject.Length];

                foreach (Object light in lightObjects)
                {
                    GameObject temp = (GameObject)light;
                    lights[i] = temp.GetComponent<Light>();
                    onLights[i] = false;
                    lights[i].color = Color.red;
                    i++;
                }

                i = 0;

                foreach (Object switc in switchObject)
                {
                    switches[i] = (GameObject)switc;
                    i++;
                }
                break;
            }

            case puzzleTypes.pressurePlateWeight:
            {
                weight = 0;
                break;
            }
        }
    }

    public void LightChange(bool[] change)
    {
        if (type == puzzleTypes.lightGame)
        {
            bool allGreen = true;
            int i = 0;

            foreach(Light light in lights)
            {
                if (change[i] == true)
                {
                    if (onLights[i])
                    { 
                        light.color = Color.red;
                        onLights[i] = false;
                    }
                    else
                    {
                        light.color = Color.green;
                        onLights[i] = true;
                    }
                }
                if (!onLights[i]) allGreen = false;
                i++;
            }

            if (allGreen) puzzleCompleted = true;
            else puzzleCompleted = false;

            puzzleTrigger.CheckIfPuzzlesAreDone();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(type == puzzleTypes.pressurePlateSimple)
        {
            if (other.name == puzzleSolver.name)
            {
                puzzleCompleted = true;
                puzzleTrigger.CheckIfPuzzlesAreDone();
            }
        }

        if (type == puzzleTypes.pressurePlateParticle)
        {
            if (other.name == puzzleSolver.name)
            {
                GameObject kubas = other.gameObject;
                kubas.transform.GetChild(0).GetComponent<Renderer>().material = emissiveMaterial;

                puzzleCompleted = true;
                puzzleTrigger.CheckIfPuzzlesAreDone();
            }
        }

        if (type == puzzleTypes.pressurePlateWeight)
        {
            if (other.transform.GetChild(0).tag == "Weight_Cube")
            {
                GameObject kubas = other.gameObject;
                weight += kubas.GetComponent<Rigidbody>().mass;
                kubas.transform.GetChild(0).GetComponent<Renderer>().material = emissiveMaterial;

                if(correctWeight == weight)
                {
                    puzzleCompleted = true;
                    puzzleTrigger.CheckIfPuzzlesAreDone();
                }
                else
                {
                    puzzleCompleted = false;
                    puzzleTrigger.CheckIfPuzzlesAreDone();
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (type == puzzleTypes.pressurePlateSimple)
        {
            if (other.name == puzzleSolver.name)
            {
                puzzleCompleted = false;
                puzzleTrigger.CheckIfPuzzlesAreDone();
            }
        }

        if (type == puzzleTypes.pressurePlateParticle)
        {
            if (other.name == puzzleSolver.name)
            {
                GameObject kubas = other.gameObject;
                kubas.transform.GetChild(0).GetComponent<Renderer>().material = nonEmissiveMaterial;

                puzzleCompleted = false;
                puzzleTrigger.CheckIfPuzzlesAreDone();
            }
        }

        if (type == puzzleTypes.pressurePlateWeight)
        {
            if (other.transform.GetChild(0).tag == "Weight_Cube")
            {
                GameObject kubas = other.gameObject;
                weight -= kubas.GetComponent<Rigidbody>().mass;
                kubas.transform.GetChild(0).GetComponent<Renderer>().material = nonEmissiveMaterial;
                //Debug.Log("svoris= " + weight);

                if (correctWeight == weight)
                    puzzleCompleted = true;
                else
                    puzzleCompleted = false;

                puzzleTrigger.CheckIfPuzzlesAreDone();
            }
        }
    }
}
