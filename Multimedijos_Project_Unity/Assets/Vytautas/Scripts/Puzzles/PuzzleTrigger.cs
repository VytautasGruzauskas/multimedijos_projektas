﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTrigger : MonoBehaviour
{
    //public
    public Puzzle[] Puzzles;
    public PuzzleDone puzzleDoneScript;

    //private
    private bool puzzleDone = false;
    

    private void Start()
    {
       // puzzleDoneScript = GameObject.FindGameObjectWithTag("Puzzle_Controller").GetComponent<PuzzleDone>();
    }

    public void CheckIfPuzzlesAreDone()
    {
        puzzleDone = true;
        for (int i = 0; i < Puzzles.Length; i++)
        {
            if (!Puzzles[i].puzzleCompleted) { puzzleDone = false; break; }
        }

        if (puzzleDone)
        {
            puzzleDoneScript.PuzzleSolved();
        }
        else
        {
            if (puzzleDoneScript.CheckIfPuzzlesAreSolved())
            {
                puzzleDoneScript.PuzzleUnSolved();
            }
            else
            {
                puzzleDoneScript.PuzzleStillNotSolved();
            }
        }
    }
}
