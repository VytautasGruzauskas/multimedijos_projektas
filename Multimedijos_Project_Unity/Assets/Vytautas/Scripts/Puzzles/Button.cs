﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    //public
    public bool[] lightChange;

    //private
    Puzzle puzzle;

    void Start()
    {
        puzzle = GetComponentInParent<Puzzle>();
    }

    public void OnPressed()
    {
        puzzle.LightChange(lightChange);
    }

    public void OnPressedPlanet(GameObject gameObject)
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
    }
}
