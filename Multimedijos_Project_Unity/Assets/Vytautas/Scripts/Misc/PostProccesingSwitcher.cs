﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;

public class PostProccesingSwitcher : MonoBehaviour
{
    public PostProcessingProfile HighProfile;
    public PostProcessingProfile NormalProfile;
    public PostProcessingProfile LowProfile;

    public PostProcessingProfile DesertHighProfile;
    public PostProcessingProfile DesertNormalProfile;
    public PostProcessingProfile DesertLowProfile;

    public PostProcessingProfile CableHighProfile;
    public PostProcessingProfile CableNormalProfile;
    public PostProcessingProfile CableLowProfile;

    void Start ()
    {
        PostProcessingBehaviour ppb = GetComponent<PostProcessingBehaviour>();

        //2 = High
        //1 = Normal
        //0 = Low
        switch (QualitySettings.GetQualityLevel())
        {
            case 0:
                if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Desert_Scene"))
                {
                    ppb.profile = DesertLowProfile;
                }
                else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("test_Scena"))
                {
                    ppb.profile = DesertLowProfile;
                }
                else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Cable_Scene"))
                {
                    ppb.profile = CableLowProfile;
                }
                else
                {
                    ppb.profile = LowProfile;
                }
                break;

            case 1:
                if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Desert_Scene"))
                {
                    ppb.profile = DesertNormalProfile;
                }
                else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("test_Scena"))
                {
                    ppb.profile = DesertNormalProfile;
                }
                else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Cable_Scene"))
                {
                    ppb.profile = CableNormalProfile;
                }
                else
                {
                    ppb.profile = NormalProfile;
                }
                break;

            case 2:
                if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Desert_Scene"))
                {
                    ppb.profile = DesertHighProfile;
                }
                else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("test_Scena"))
                {
                    ppb.profile = DesertHighProfile;
                }
                else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Cable_Scene"))
                {
                    ppb.profile = CableHighProfile;
                }
                else
                {
                    ppb.profile = HighProfile;
                }
                break;
        }
    }
}
