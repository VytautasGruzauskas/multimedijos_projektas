﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lava : MonoBehaviour
{
    public float speed = 0.5f;
    public float floor = 0.3f;
    public float ceiling = 1.0f;

    void Update ()
    {
        Renderer renderer = GetComponent<Renderer>();
        Material mat = renderer.material;

        
        float emission = floor + Mathf.PingPong(Time.time * speed, ceiling - floor);
        Color baseColor = Color.red; //Replace this with whatever you want for your base color at emission level '1'

        Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);

        mat.SetColor("_EmissionColor", finalColor);
    }
}
