﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rope : MonoBehaviour
{
    internal Rigidbody RBody;
	// Use this for initialization
	internal void Start ()
    {
        gameObject.AddComponent<Rigidbody>();
        RBody = gameObject.GetComponent<Rigidbody>();
        RBody.isKinematic = false;
        RBody.mass = 0.1f;
        int childCount = transform.childCount;
        
        for(int i = 0; i < childCount; i++)
        {
            Transform t = transform.GetChild(i);

            t.gameObject.AddComponent<HingeJoint>();

            HingeJoint hinge = t.gameObject.GetComponent<HingeJoint>();

            hinge.connectedBody = i == 0 ? RBody : transform.GetChild(i - 1).GetComponent<Rigidbody>();

            hinge.useSpring = true;
            hinge.enableCollision = true;
        }	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
