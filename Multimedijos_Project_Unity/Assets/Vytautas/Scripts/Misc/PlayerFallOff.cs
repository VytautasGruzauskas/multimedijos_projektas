﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerFallOff : MonoBehaviour
{
    public GameObject player;
    public GameObject[] fallOffObjects;


    // Use this for initialization
    void Start()
    {

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            PlayerPrefs.SetInt("Next_Scene", SceneManager.GetActiveScene().buildIndex);
            SceneManager.LoadScene("Loading_Scene");
        }

        foreach(GameObject obj in fallOffObjects)
        {
            if (other.gameObject == obj)
            {
                obj.transform.position = new Vector3(player.transform.position.x,
                    player.transform.position.y+5, player.transform.position.z);
            }
        }

    }
}
