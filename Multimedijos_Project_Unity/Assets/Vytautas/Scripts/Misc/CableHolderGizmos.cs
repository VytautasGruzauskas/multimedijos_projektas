﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableHolderGizmos : MonoBehaviour
{
    public enum tipai
    {
        Front,
        Back,
        Right,
        Left
    };

    public tipai ImageTypes;


    private void OnDrawGizmos()
    {
        if(ImageTypes == tipai.Front)
            Gizmos.DrawIcon(transform.position, "Front.png");
        else if (ImageTypes == tipai.Back)
            Gizmos.DrawIcon(transform.position, "Back.png");
        else if (ImageTypes == tipai.Right)
            Gizmos.DrawIcon(transform.position, "Right.png");
        else if (ImageTypes == tipai.Left)
            Gizmos.DrawIcon(transform.position, "Left.png");

        Gizmos.color = Color.yellow;
        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        Gizmos.matrix = rotationMatrix;
       

        //if (isblocked == 0) Gizmos.color = Color.yellow; else Gizmos.color = Color.red;
        Gizmos.DrawWireCube(Vector3.zero, Vector3.one);

    }
}
