﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ateroids_Move : MonoBehaviour
{
    //public
    public GameObject[] asteroids;
    public float speed = 10f;

    //private
    

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        foreach(GameObject asteroid in asteroids)
        {
            transform.Rotate(Vector3.up * Time.deltaTime * speed, Space.Self);
        }
        
    }
}
