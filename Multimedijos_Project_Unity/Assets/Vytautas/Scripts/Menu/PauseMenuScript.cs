﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour
{
    //public
    public static bool GameIsPaused = false;
    public GameObject PauseMenu;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller;
	public bool Lygis3D = true; 

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        Time.timeScale = 1f;
        PauseMenu.SetActive(false);
        GameIsPaused = false;
		// išėmiau, nes pas mane (Domantą) lygyje neveikia su šituo skript'as 
		if (Lygis3D) { 
			controller.enabled = true;
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
		}
    }


    void Pause()
    {
        PauseMenu.SetActive(true);
        GameIsPaused = true;
		// išėmiau, nes pas mane (Domantą) lygyje neveikia su šituo skript'as 
		if(Lygis3D)
			controller.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 0f;
    }

    public void PressedMenu()
    {
        Resume();
        PlayerPrefs.SetInt("Next_Scene", 0);
        SceneManager.LoadScene("Loading_Scene");
    }

    public void PressedQuit()
    {
        Application.Quit();
    }
}
