﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //public
    public GameObject[] ParticleSystems;
    public GameObject ContinueButton;
    public GameObject NewGameButton;
    public GameObject OptionsButton;
    public GameObject[] optionsButtons;
    public GameObject currentOption;
    public GameObject ExitButton;
    //public GameObject LoadingScreen;

    //private
    private bool loadScene = false;
    private bool optionPress = false;
    private bool optionSelected = false;


    private string[] optionText = { "Selected\nLow", "Selected\nMedium", "Selected\nHigh" };
    
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        Text currentOptionText = currentOption.GetComponent<Text>();
        currentOptionText.text = optionText[QualitySettings.GetQualityLevel()]; 
        currentOption.SetActive(false);
    }


    public void PressedContinue()
    {
        if (!loadScene)
        {
            int level = PlayerPrefs.GetInt("Continue_Level");
            if (level < 2 || level > 6)
                PlayerPrefs.SetInt("Continue_Level", 2);

            loadScene = true;
            PlayerPrefs.SetInt("Next_Scene", PlayerPrefs.GetInt("Continue_Level"));
            SceneManager.LoadScene("Loading_Scene");
        }
    }

    public void PressedStart()
    {
        if (!loadScene)
        {
            loadScene = true;
            PlayerPrefs.SetInt("Next_Scene", 2);
            PlayerPrefs.SetInt("Continue_Level", 2);
            SceneManager.LoadScene("Loading_Scene");
        }
    }

    public void PressedOptions()
    {
        if (!loadScene)
        {
            if (!optionPress)
            {
                ContinueButton.SetActive(false);
                NewGameButton.SetActive(false);
                ExitButton.SetActive(false);
                foreach (GameObject button in optionsButtons)
                {
                    button.SetActive(true);
                }
                optionPress = true;
                optionSelected = true;
                ParticleSystems[1].SetActive(true);
                currentOption.SetActive(true);
            }
            else
            {
                ContinueButton.SetActive(true);
                NewGameButton.SetActive(true);
                ExitButton.SetActive(true);
                foreach (GameObject button in optionsButtons)
                {
                    button.SetActive(false);
                }
                optionPress = false;
                optionSelected = false;
                ParticleSystems[1].SetActive(false);
                currentOption.SetActive(false);
            }
            //Debug.Log("Paspaude options");
        }
    }

    public void PressedOptionsQuality(int qualityLevel)
    {
        //2 = High
        //1 = Normal
        //0 = Low

        Text currentOptionText = currentOption.GetComponent<Text>();

        switch (qualityLevel)
        {
            case 2://High
                QualitySettings.SetQualityLevel(qualityLevel, true);
                break;


            case 1://Medium
                QualitySettings.SetQualityLevel(qualityLevel, true);
                break;


            case 0://Low
                QualitySettings.SetQualityLevel(qualityLevel, true);
                break;
        }

        currentOptionText.text = optionText[QualitySettings.GetQualityLevel()];


    }


    public void PressedExit()
    {
        if (!loadScene)
        {
            Application.Quit();
        }
    }

    //laikina
    public void Pressed2D()
    {
        if (!loadScene)
        {
            SceneManager.LoadScene("14");
        }
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        //do stuff

        GameObject button = eventData.pointerEnter.transform.parent.gameObject;

        //Debug.Log("uzvede ant mygtuko "+ button.name);
        switch (button.name)
        {
            case "Continue":
                {
                    ParticleSystems[0].SetActive(true);
                    break;
                }

            case "New Game":
                {
                    ParticleSystems[0].SetActive(true);
                    break;
                }


            case "Options":
                {
                    if(!optionSelected)
                        ParticleSystems[1].SetActive(true);
                    break;
                }

            case "Exit":
                {
                    ParticleSystems[2].SetActive(true);
                    break;
                }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GameObject button = eventData.pointerEnter.transform.parent.gameObject;

        //Debug.Log("isejo is mygtuko " + button.name);
        if (!loadScene)
        {
            switch (button.name)
            {
                case "Continue":
                    {
                        ParticleSystems[0].SetActive(false);
                        break;
                    }

                case "New Game":
                    {
                        ParticleSystems[0].SetActive(false);
                        break;
                    }

                case "Options":
                    {
                        if (!optionSelected)
                            ParticleSystems[1].SetActive(false);
                        break;
                    }

                case "Exit":
                    {
                        ParticleSystems[2].SetActive(false);
                        break;
                    }
            }
        }   
    }

    /*
    IEnumerator LoadNewScene()
    {
        yield return new WaitForSeconds(3);


        AsyncOperation async = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);

        while(!async.isDone)
        {
            yield return null;
        }
    }*/
}
