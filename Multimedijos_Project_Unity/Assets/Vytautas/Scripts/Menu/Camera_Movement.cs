﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Movement : MonoBehaviour
{

    public float speed = 10f;
   

	// Use this for initialization
	void Start ()
    {
     
		

	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * speed, Space.Self);
    }
}
