﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Press4Hint : MonoBehaviour {

    //tipo veikia bet dar reik pazet su error at runtime tada ok bus
    public int hintSize = 4;
    public GameObject[] HintObject;
    public GameObject[] UIHint;
    public GameObject useKeyHint;

    //public int hintID;

    //private
    private bool[] pressedHintObject;
    private float reach;
    private Transform mainCamera;
    private bool showingHint = false;
    int layerMask = 1 << 8;


    void OnValidate()
    {
        if(UIHint.Length != hintSize)
            UIHint = new GameObject[hintSize];

        if (HintObject.Length != hintSize)
            HintObject = new GameObject[hintSize];
    }

    // Use this for initialization
    void Start ()
    {
        pressedHintObject = new bool[HintObject.Length];

        for (int i = 0; i < pressedHintObject.Length; i++)
            pressedHintObject[i] = false;

        showingHint = false;
        reach = GameObject.Find("FirstPersonCharacter").GetComponent<AddedControls>().reachDistance - 1;
        mainCamera = GameObject.Find("FirstPersonCharacter").GetComponent<AddedControls>().mainCamera;
        useKeyHint.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(mainCamera.position, mainCamera.forward);
        RaycastHit hit;

        if (useKeyHint.activeInHierarchy)
            useKeyHint.SetActive(false);

        if (Physics.Raycast(ray, out hit, reach, layerMask))
        {
            if (hit.collider.tag == "Hint")
            {

                if (!showingHint && !useKeyHint.activeInHierarchy)
                    useKeyHint.SetActive(true);
                else if(showingHint && useKeyHint.activeInHierarchy)
                    useKeyHint.SetActive(false);


                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (!showingHint)
                    {
                        for (int i = 0; i < hintSize; i++)
                        {
                            if (HintObject[i].activeSelf)
                                if (HintObject[i].name == hit.collider.gameObject.name)
                                {
                                    pressedHintObject[i] = true;
                                    UIHint[i].SetActive(true);
                                    showingHint = true;

                                    for (int j = 0; i < pressedHintObject.Length; i++)



                                    StartCoroutine(WaitForSec(i));
                                    break;
                                }
                        }
                    }
                    //Debug.Log("pavyko");
                }
            }
        }
    }
    
    // use real time seconds
    IEnumerator WaitForSec(int index)
    {
        yield return new WaitForSeconds(10);
        showingHint = false;
        UIHint[index].SetActive(false);
    }
    
}
