﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastHintAppear : MonoBehaviour {

    public GameObject[] hints;
    public GameObject hintATS;
    public float smoothtime = 0.3F;
    public float height;

    private int tmp1 = 0;
    private int tmp2 = 0;
    private int tmp3 = 0;
    private int tmp4 = 0;
    private Vector3 velocity = Vector3.zero;

    // Use this for initialization
    void Start () {

        //for(int i = 0; i < hints.Length; i++)
        //{
        //    hints[i].SetActive(false);
        //}

        
    }
	
	// Update is called once per frame
	void Update () {

        if (hints[0].activeSelf)
        {
            tmp1 = 1;
        }

        if (hints[1].activeSelf)
        {
            tmp2 = 1;
        }

        if (hints[2].activeSelf)
        {
            tmp3 = 1;
        }

        if (hints[3].activeSelf)
        {
            tmp4 = 1;
        }

        if (tmp1 == 1 && tmp2 == 1 && tmp3 == 1 && tmp4 == 1)
        {
            hintATS.SetActive(true);
        }
    }
}
