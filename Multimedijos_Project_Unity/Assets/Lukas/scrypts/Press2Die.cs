﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Press2Die : MonoBehaviour {

    //tipo veikia bet dar reik pazet su error at runtime tada ok bus
    public int numbeoftext = 1;
    public GameObject[] DeathObject;
    public GameObject useKeyHint;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller;
    public AudioClip deathaudio;
    public AudioSource deathAudioSource;


    //private
    private float reach;
    private Transform mainCamera;
    private bool showingHint = false;
    int layerMask = 1 << 8;
    private Scene scene;


    void OnValidate()
    {

    }

    // Use this for initialization
    void Start ()
    {
        showingHint = false;
        reach = GameObject.Find("FirstPersonCharacter").GetComponent<AddedControls>().reachDistance - 1;
        mainCamera = GameObject.Find("FirstPersonCharacter").GetComponent<AddedControls>().mainCamera;
        useKeyHint.SetActive(false);
        controller.GetComponentInChildren<Animator>().enabled = false;

        //grojam mirties garsa
        deathAudioSource.clip = deathaudio;

        //gaunam scena
        scene = SceneManager.GetActiveScene();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(mainCamera.position, mainCamera.forward);
        RaycastHit hit;

        if (useKeyHint.activeInHierarchy)
            useKeyHint.SetActive(false);

        if (Physics.Raycast(ray, out hit, reach, layerMask))
        {
            if (hit.collider.tag == "Death")
            {

                if (!showingHint && !useKeyHint.activeInHierarchy)
                    useKeyHint.SetActive(true);
                else if(showingHint && useKeyHint.activeInHierarchy)
                    useKeyHint.SetActive(false);


                if (Input.GetKeyDown(KeyCode.E))
                {
                    //if (!showingHint)
                    //{
                    controller.enabled = false;
                    //mainCamera.transform.position = Vector3.Lerp(this.transform.position,
                    //new Vector3(this.transform.position.x, this.transform.position.y-7f, this.transform.position.z), Time.deltaTime)
                    //}
                    controller.GetComponentInChildren<Animator>().enabled = true;
                    deathAudioSource.Play();
                    StartCoroutine(WaitForSec());
                    
                }
            }
        }
    }

    IEnumerator WaitForSec()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(scene.name);
    }
}
