﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Effects
{
    public class randomExplo : MonoBehaviour
    {

        void Start()
        {
            float explosionForce = UnityEngine.Random.Range(0.1f, 1.0f);
            //Debug.Log("force: "+ explosionForce);
            StartCoroutine(StartEx(explosionForce));
        }

        private IEnumerator StartEx(float force)
        {
            // wait one frame because some explosions instantiate debris which should then
            // be pushed by physics force
            yield return null;


            float multiplier = GetComponent<ParticleSystemMultiplier>().multiplier;
            //float rnd = UnityEngine.Random.Range(1, 20);

            float r = 10 * multiplier;
            var cols = Physics.OverlapSphere(transform.position, r);
            var rigidbodies = new List<Rigidbody>();
            foreach (var col in cols)
            {
                if (col.attachedRigidbody != null && !rigidbodies.Contains(col.attachedRigidbody))
                {
                    rigidbodies.Add(col.attachedRigidbody);
                }
            }
            foreach (var rb in rigidbodies)
            {
                rb.AddExplosionForce(force * multiplier, transform.position, r, 1 * multiplier, ForceMode.Impulse);
            }
        }
    }
}
