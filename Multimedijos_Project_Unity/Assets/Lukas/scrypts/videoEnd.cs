﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class videoEnd : MonoBehaviour {

    public VideoPlayer vid1;
    public Canvas canv1;
    public GameObject text;
	// Use this for initialization
	void Start () {
        text.SetActive(false);
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(vid1.isPlaying)
        {
            canv1.enabled = false;
            StartCoroutine(WaitForSec());
            text.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                vid1.enabled = false;
                canv1.enabled = true;
                text.SetActive(false);
            }

        }
	}

    IEnumerator WaitForSec()
    {
        yield return new WaitForSeconds(28);
        vid1.enabled = false;
        canv1.enabled = true;
        text.SetActive(false);
    }
}
