﻿using UnityEngine;
using System.Collections;

// prieso sustabdymas 
public class EJump : MonoBehaviour
{

    //Platform options
    [HideInInspector]
    public bool facingRight = true;
    [HideInInspector]
    public bool jump = true;

    public float velocity = 1f;

    public Transform sightStart;
    public Transform sightEnd;

    //Platform options
    public float jumpForce = 1000f;

    //Platform options
    public Transform groundCheck;

    //Platform options
//    private bool grounded = false;
    private Animator anim;
    private Rigidbody2D rb2d;

    //Enemy options
    public LayerMask detectWhat;

    //Enemy options
    public bool colliding;

    public int numberofchild;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    //Susikuriame zymekli kuris liecia dalykus ir jei jie uzfiksuoti kaip "Pazinimo sodo vaisiai"
    //tuomet draudzia tai pasiekti ir priesas apsisuka. 
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, GetComponent<Rigidbody2D>().velocity.y);

        if (transform.childCount == numberofchild)
        {

            colliding = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);

            if (colliding)
            {
                //blogas apvertimas
                //transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y * -1);

                velocity *= -1;
                Flip();

                //ikelt is spcontroller
                //grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
                // jump = true;
            }
        }
        else
        {
            velocity = 0;
            //kazko neveikia, galima krentancias grindinis daryt nu kazka idomaus
            rb2d.isKinematic = false;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        Gizmos.DrawLine(sightStart.position, sightEnd.position);
    }

    void FixedUpdate()
    {
        if (jump)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));

            jump = true;
        }
    }

    //metodas, kuris apsuka objektus visualiai
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
