﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class FallToDeath : MonoBehaviour {

    /// <summary>
    /// DeathFallGeneric
    /// </summary>
    [HideInInspector]
    public float lastYPosition;
    [HideInInspector]
    public float lastYTravelDistance;

    /// <summary>
    /// DeathFallGeneric
    /// </summary>
    public float fallHeight = 0f;
    public float deathHeight = 1000f;

    private bool grounded = false;

    public Transform groundCheck;

	// reikalingas manipuliacijai rigibodzio 
	private Rigidbody2D rb2d = null;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        DeathFallGeneric(); 

	}


    //Kritiminis nužudymas
    void DeathFallGeneric()
    {
        if (!grounded)
        {
            //calculate the distance between our current height and the height we were in the last frame
            lastYTravelDistance = rb2d.position.y - lastYPosition;

            //if the difference is negative, it means we're descending 
            fallHeight += lastYTravelDistance < 0 ? lastYTravelDistance : 0;
        }
        else
        {
            //we check to see if we passed the allowed falling distance and kill the player if necessary
            if (fallHeight >= -deathHeight)
            {
                KillPlayer();
            }

            //reset fall height since we landed (doesn't matter if we're dead or alive)
            fallHeight = 0;
        }

        //cache our current Y position for comparison in the next frame
        lastYPosition = rb2d.position.y;
    }

    void KillPlayer()
    {
        //Application.LoadLevel(Application.loadedLevel);
        SceneManager.LoadScene("14"); 
    }
}
