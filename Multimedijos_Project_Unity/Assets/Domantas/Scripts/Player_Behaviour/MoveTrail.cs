﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// kulkos judėjimo metodas 
public class MoveTrail : MonoBehaviour {

	//[HideInInspector] public float moveSpeed = 1; 

	// Update is called once per frame
	void Update () {
		// vietoj rigidbody, kuri as norejau naudoti :) tačiau eis i tik i vieną
		// direkciją :'( 
		transform.Translate(Vector3.up * Time.deltaTime * 50); 
		Destroy (gameObject, 3); 
	}

	// kad kulka neitų kiaurai 
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Enemy") 
			Destroy (gameObject); 
	}
		
}
