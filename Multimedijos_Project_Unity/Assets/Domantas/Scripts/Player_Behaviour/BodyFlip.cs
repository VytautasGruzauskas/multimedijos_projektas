﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyFlip : MonoBehaviour {

    public int rotationOffset = 90;

    private float x; 
	private Vector3 ls;
	private Vector3 dir; 

    private int changer = 90;

    // sisteminiai kintamieji, neleidžia flipinti beriekalo; Semafora tam tikri 
    private bool cont1;
    private bool cont2; 

    void Start()
    {
		dir = Input.mousePosition - Camera.main.WorldToScreenPoint (transform.position); 
        cont1 = true;
        cont2 = true; 
        x = transform.localScale.x;
        ls = transform.localScale;
    }

    void Update()
    {
        dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);

        //float rotZ = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;

        if (dir.x >= 0)
        {
            if (cont1 && !facingRight)
                Flip(); 
            cont1 = false;
            cont2 = true;

            //transform.rotation = Quaternion.Euler(0f, 0f, rotZ - 90);

            //ls.x = x;
            //transform.localScale = ls;
        }
        else
        {
            if (cont2 && facingRight)
                Flip(); 
            cont2 = false;
            cont1 = true;

            //transform.rotation = Quaternion.Euler(0f, 0f, rotZ - 90);
            //ls.x = -x;
            //transform.localScale = ls;
        }
    }

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public float h;

    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {

        // ašis 
        h = Input.GetAxis("Horizontal");

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();
    }


    //metodas, kuris apsuka objektus visualiai
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        changer = -changer; 
    }
}
