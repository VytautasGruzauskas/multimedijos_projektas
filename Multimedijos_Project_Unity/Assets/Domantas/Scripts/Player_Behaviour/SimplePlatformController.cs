﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement; 

[RequireComponent(typeof(AudioSource))]
public class SimplePlatformController : MonoBehaviour
{

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    //antras šuolis
    [HideInInspector] public bool jumpspec = false;
    //antro šuolio sąlygos kintamasis
    [HideInInspector] public bool JumpCondition = false;
    // vėliavėlė, kuri tampa false, kai veikėjas žūsta (reikalinga apriboti veikėjo veiksmams po žuties) 
    [HideInInspector] public bool alive = true; 
    [HideInInspector] public float h;
    [HideInInspector] public bool moving = false; 
	// nesuveikė - tai pradeda galioti visiems ... 
	//[HideInInspector] public bool EnemyStopped = false; 
	[HideInInspector] public bool ColliderIsOff = true; 
	[HideInInspector] public int bottleCounter = 0; 
	[HideInInspector] public int liquidCounter = 0; 
	[HideInInspector] public bool immortal = false; 
	[HideInInspector] public bool immortal_2 = false; 
	[HideInInspector] private bool groundVirgin = true; 
	[HideInInspector] public bool portal_escape = false; 
	[HideInInspector] bool transition = true; 
	[HideInInspector] bool antiLightning = false; 
	[HideInInspector] bool shotOfGreen = false; 
	[HideInInspector] bool NotCalled = false; 

	[HideInInspector] public float state = 0; 
	[HideInInspector] public float state2 = 0;
	[HideInInspector] float timeLeft = 100000000; 

	[HideInInspector] int RANGE2 = 250; 
	[HideInInspector] int RANGE1 = 1000; 
	[HideInInspector] int range; 

	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%SĄSAJA%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	private EnemyFollow anotherscript; 

	public Camera camera; 
	public Camera camera2; 
	public Camera camera3; 
	public Camera camera4; 

	public GameObject dimension; 

	AudioSource audiosource; 
	AudioSource audiosource_2; 

 //   private bool grounded = false;
    private Animator anim;
    private Rigidbody2D rb2d;

    /// <summary>
    /// DeathFall
    /// </summary>
    [HideInInspector] public float lastYVelocityDistance;
    // skirtas deathfall'ui 
    [HideInInspector] public float lastYVelocity;
    [HideInInspector] public float greitis;

    public float moveForce = 365f;
    public float maxSpeed = 1f;
    //public float jumpForce = 1000f;
	public float jumpForce = 700f; 

    bool Jump = true;

    /// <summary>
    /// DeathFall
    /// </summary>
    // public float fallVelocity = 0f;
    public float deathVelocity = 1000f;

    public Transform groundCheck;
    public int i = 0;

    /// <summary>
    /// garso laukai 
    /// </summary>
    public AudioClip touchedbyground; 
    public AudioClip jumpfromtheground; 
    public AudioClip jumpintheair; 
    public AudioClip shit;
    public AudioClip walks;
    public AudioClip electricity_sound; 
	public AudioClip scaryMusic; 
	public AudioClip runSoundEffect; 

	// Rodomo tekstai 
    public Text scoreText; 
	public Text rangeText; 
	public Text timer; 
	public Text run; 



    /// <summary>
    /// animacijos aktivatorius
    /// </summary>
    public attackAnimation _AttackAnimation;
    public spinAnimation _SpinAnimation;

    void Start()
    {
		audiosource = GetComponent<AudioSource>(); 
		//pradinis buteliukų skaičius 
		bottleCounter = 5; 
		run.text = ""; 

		// pradinis view nustatytas nustatymuose 
		state = Camera.main.fieldOfView; 

		// pradine kameros stadija 
		Camera.main.fieldOfView = 25.0F; 

		// paimam tarpdimensinio priešo skriptą 
		anotherscript = dimension.GetComponent<EnemyFollow> (); 

		// laikas iki prieso pasirodymo 
		timeLeft = 300F; 

		// ijungta tik pirma kamera pradzioje 
		camera.enabled = true; 
		camera2.enabled = false; 
		camera3.enabled = false; 
		camera4.enabled = false; 

		Cursor.visible = true; 
    }

    // Use this for initialization
    void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

	// Nepriklausomas nuo frame rate update 
	void FixedUpdate()
	{

		// sugražina kamera į įprastą poziciją
		if (transition) {
			if (Camera.main.fieldOfView < state)
				Camera.main.fieldOfView = Camera.main.fieldOfView + 1F; 
			if (Camera.main.fieldOfView > state)
				Camera.main.fieldOfView = Camera.main.fieldOfView - 1F; 
		}

        // jeigu gyvas (kad nebūtų vaikčiojantis lavonas)  
        if (alive)
            MOVE();
	}

	// Update is called once per frame (classic) 
    void Update()
    {

		// Collider control (HeroCollider netinka, nes tada veikėjas pasidaro permatomas ) 
		Collider2D WeaponCollider = gameObject.transform.Find ("Weapon").GetComponent<Collider2D> (); 

        // ground checker - alternatyva trigeriui (taciau cia didelis velinimas)  
        // grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("ground"));

        // greicio value zymeklis 
        //lastYVelocity = rb2d.velocity.y; 
        //parodo y greitį !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//scoreText.text = "" + greitis.ToString();  // make it a string to output to the Text object 

		range = (int)(Math.Sqrt (Math.Pow ((this.transform.position.x) - (dimension.transform.position.x), 2) + Math.Pow ((this.transform.position.y) - (dimension.transform.position.y), 2))); 

		Canvas_Controls (); 

		Interdimensional_Enemy_Camera_Effects (); 

		if ((timeLeft < 0)&&(groundVirgin)) {
			Interdimensional_Enemy_Awake (); 
			Interdimensional_Enemy_Show_Up (); 
		}

        // tikrina tagą 
        //if (gameObject.CompareTag("Enemy"))
        //{
        //    AudioSource.PlayClipAtPoint(shit, new Vector3(rb2d.position.x, rb2d.position.y, 0));
        //}

        // (x)(x)(x)(x)(x)(x)(x)(x)(x)(x)(x)(x)(x)mygtukai(x)(x)(x)(x)(x)(x)(x)(x)(x)(x)(x)(x)(x)

		if (alive) {

			var weapon = gameObject.transform.Find ("Weapon").gameObject;
			var gun = gameObject.transform.Find ("Gun").gameObject;
			var bottle = gameObject.transform.Find("Bottle").gameObject; 

			// šuolis nuo žemės 
			if ((Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.Joystick1Button0)) && Jump) {
				AudioSource.PlayClipAtPoint (jumpfromtheground, new Vector3 (rb2d.position.x, rb2d.position.y, 0));

				JUMP ();
				Jump = false;
				// pakeicia salygą double jumpui; 
				Invoke ("JumpConditionMethod", 0.1f);
			}

			// dviugbo šuolio galimybė
			if ((Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.Joystick1Button0)) && JumpCondition) {
				AudioSource.PlayClipAtPoint (jumpintheair, new Vector3 (rb2d.position.x, rb2d.position.y, 0));
				// antras "silpnesnis" suolis  
				DOUBLEJUMP ();
			}

			if (ColliderIsOff) {
				WeaponCollider.enabled = false; 
			}

			// |||Valdymas|||
			// puolama su klavišu F, pelytės kairuoju klavišu arba X360 kontroleryje (X) 
			if (Input.GetKeyDown (KeyCode.F) || Input.GetMouseButtonDown (0) || Input.GetKey (KeyCode.Joystick1Button2)) {
				WeaponCollider.enabled = true;
				ColliderIsOff = false; 
				// ginklų switch'as 
				var child1 = gameObject.transform.Find ("Weapon").gameObject;
				child1.SetActive (true);
				var child2 = gameObject.transform.Find ("Gun").gameObject;
				child2.SetActive (false);

				//anim.SetTrigger("Smash"); 
				//geras pavyzdys nuorodos į kitą skriptą tame pačiame objekte 
				_AttackAnimation.attack ();

				Invoke ("WeaponColliderTurnOff", 1f);
			}

			// mėtomi buteliukas klavišu G, dešiniuoju pelės klavišu arba X360 kontroleryje (O) 
			if (Input.GetKeyDown (KeyCode.G) || Input.GetMouseButtonDown (1) || Input.GetKeyDown (KeyCode.Joystick1Button3)) {
				// Shoot(); 

				// ginklų switch'as 
				gun.SetActive (true);
				weapon.SetActive (false);
			}

			// restartuojamas lygis 
			if (Input.GetKeyDown (KeyCode.K) || Input.GetKeyDown (KeyCode.Joystick1Button5)) {
				KillPlayer3 ();

				// ginklų switch'as 
				gun.SetActive (false);
				weapon.SetActive (false);
			}
				
			if (Input.GetKeyDown (KeyCode.LeftAlt) && bottleCounter > 0) { 
				SuperJump (weapon, gun, bottle); 
			}

			if (!immortal_2 && Input.GetKeyDown (KeyCode.Q) && bottleCounter>=10) 
			{
				ImmortalMode (weapon, gun, bottle); 
			}

			// TOGGLE (leidžią naudoti jei turi buteliukų) 
			if (Input.GetKeyDown (KeyCode.E) && bottleCounter > 0) {
				antiLightning = !antiLightning; 
				NotCalled = true; 
			}
			
			float smth = Time.time * 10.0F; 
			int life = (int) (smth);
			// Debug.Log (life); 

			// jei žmogeliukas nemirtingas, ir jei paspaudė mygtuką 
			if (!immortal && antiLightning) 
			{
				// nuiminėja buteliukus po sekundę 
				//bottleCounter -= timeInSeconds; 
				if ((life % 10 == 0) && (NotCalled)) {
					bottleCounter -= 1; 
					// Debug.Log ("Nuskaite"); 
					NotCalled = false; 
				}

				if (((life + 1) % 10 == 0)) {
					NotCalled = true; 
					// Debug.Log ("Laukia"); 
				}

				// atliekama animacija ir veiksmai 
				if (shotOfGreen == false) {
					weapon.SetActive (false);
					gun.SetActive (false);
					bottle.SetActive (true);

					Invoke ("StopDrink", 0.1F); 

					// aktyvuojamas chemikalų efektas 
					var child = gameObject.transform.Find ("Hero_1").transform.Find ("Electricity").gameObject; 
					child.SetActive (true);

					immortal_2 = true; 

					shotOfGreen = true; 
				}

				// išjungia galią 
				if (bottleCounter == 0) {
					AntiElectricityPowerOff (); 
				}
			}
			if (antiLightning == false) {
				AntiElectricityPowerOff (); 
			}

			// teleportas ... 
			//if (Input.GetKeyDown (KeyCode.R) && bottleCounter > 20) {
			//	bottleCounter = -20; 
			//
			//	rb2d.transform.position = new Vector3 (0.0f, 0.0f, 0.0f); 
			//}

		} 
		else if (!alive){
			// išjungiame ginklą, kad tąsai nebepridarytų žalos (reikia išjungti, 
			// nes jei nesibaigia animacija, tai dar tada gali veikėjas nužudyti priešą) 
			WeaponCollider.enabled = false; 
			// tampa ground, jeigu negyvas veikėjas (ground, nes tada jis jaucia kolizijas su zeme) 
			this.tag = "ground"; 
		}
    }

	// <-><-><-><-><-><-><-><-><-><->-<-><->trigeriai<-><-><-><-><-><-><-><-><-><->-<-><-> 

    void OnTriggerEnter2D(Collider2D other)
    {

        if (alive)
        {

			// atsarginis kameros metodas 
			//if ((other.gameObject.CompareTag("ground") && groundVirgin)) 
			//{
					//Camera.main.fieldOfView = 25.0F; 
					//Invoke("MainCameraNormal", 0.5F); 
					//groundVirgin = false; 
			//}

			// trigeris, kuris leidžia "rinkti" buteliukus 
			if (other.gameObject.CompareTag ("Bottle")) {
				bottleCounter++; 
				// Debug.Log ("Surinkta buteliukų: " + bottleCounter); 
			}

			// veikėjas nužudomas žaibu 
			if (!immortal_2 && !immortal && other.gameObject.CompareTag("Enemy_lightning"))
            {
				if (bottleCounter < 20) {
					AudioSource.PlayClipAtPoint (electricity_sound, new Vector3 (rb2d.position.x, rb2d.position.y, 0)); 

					// aktyvuoja elektra 
					var child = gameObject.transform.GetChild (0).transform.GetChild (0).gameObject;
					child.SetActive (true);

					KillPlayer ();
				} else
					// palengvinimas žaidėjui, kad jai jau surinko 20 buteliukų galėtų išgyventi vieną žaibo ataką 
					bottleCounter -= 20; 
            }

			// veikėja nužudo "Objektas" 
            if (other.gameObject.CompareTag("Enemy_Timer"))
            {
                KillPlayer2();
            }

			//if(other.gameObject.CompareTag("Head")) 
			//{
			//	EnemyStopped = true; 
			//}

			// Nužudo įprastas preišas žmogeliuką (jei paspaustas "immortal" mygtukas, tada neleidžia nužudyti)
	        if (!immortal && other.gameObject.CompareTag("Enemy"))
	        {

				KillPlayer ();

	        }

			// aktyvuojama šokinėjimas esant ant žemės 
            if (other.gameObject.CompareTag("ground"))
            {
                JumpActivate();



                if ((rb2d.velocity.y <= -deathVelocity))
                {
                    rb2d.AddForce(new Vector2(0f, -lastYVelocity * 60));

                    AudioSource.PlayClipAtPoint(touchedbyground, new Vector3(rb2d.position.x, rb2d.position.y, 0));

                    //neleidžia lavonui pritrėkštam lakstyti 
                    //rb2d.isKinematic = true; 

                    KillPlayer();
                }
            }

			// Suaktyvuoja sekančią kamerą esančią ant to tarpdimensinio priešo 
			if ((other.gameObject.layer == 11) && (groundVirgin)) {

				Interdimensional_Enemy_Show_Up (); 

			} else if (other.gameObject.layer == 11) {

				// grazinamas iprastas greitis, jei sokineja ant zemes 
				anotherscript.velocity = new Vector2 (30F, 30F); 

			}

			// jei pasiekia portalus pabėga nuo priešo ir gauna daug buteliukų 
			if (other.gameObject.CompareTag("Portal_Escape")) 
			{
				Portal_Escape (); 
			}

			if (other.gameObject.CompareTag ("Object_Zone")) 
			{
				Camera.main.fieldOfView = 150; 
				state = 120; 
				camera2.enabled = false; 
				camera3.enabled = false; 
				camera4.enabled = false; 
				Interdimensional_Enemy_Awake (); 
				Vector3 position = this.transform.position + new Vector3 (-50F, 30F, 0F); 
				anotherscript.transform.position = position; 
				anotherscript.velocity = new Vector2 (40F, 40F); 
			}

        }
    }



	// panašiai kaip trigeris, tik kad veikimo principas tęstinas 
    private void OnCollisionStay2D(Collision2D other)
    {

        //if (other.gameObject.CompareTag("Enemy_Timer"))
        //{
        //    KillPlayer2();
        //}


        if (other.gameObject.CompareTag("ground"))
        {
            //if (moving)
            //    AudioSource.PlayClipAtPoint(walks, new Vector3(rb2d.position.x, rb2d.position.y, 0));
        }

		// jeigu comperintu su human nieko nenutiktu 
    }

	// judėjimo metodas (gali būti tik fixed update zonoje) 
    void MOVE()
    {

		// ašis 
        h = Input.GetAxis("Horizontal");

		// svarbus 
        anim.SetFloat("Speed", Mathf.Abs(h));

        if (h * rb2d.velocity.x < maxSpeed)
        {
            rb2d.AddForce(Vector2.right * h * moveForce); 
        }

        //jei pasiekia max greiti kazkas buna
        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
        {
            moving = true;
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
        }
        else
            moving = false;

        // flipas perkeltas į atskirą skriptą 
        //if (h > 0 && !facingRight)
        //    Flip();
        //else if (h < 0 && facingRight)
        //    Flip();

    }

	//GALIMA REFAKTORINTI JUMPŲ KODUS 

    // simple jump metodas 
    void JUMP()
    {
        anim.SetTrigger("Jump");
        rb2d.AddForce(new Vector2(0f, jumpForce));
        //jump = true;
    }

    // double jump metodas
    void DOUBLEJUMP()
    {
        anim.SetTrigger("Jump");
        rb2d.AddForce(new Vector2(0f, jumpForce / 2));
        //kai atliekamas antras šuolis trečias nebeleidžiamas
        JumpCondition = false;
    }

	// specialus jump metodas 
	void SPECIALJUMP()
	{
		anim.SetTrigger("Jump");
		rb2d.AddForce(new Vector2(0f, jumpForce * 2));
		//kai atliekamas antras šuolis trečias nebeleidžiamas
	}

    // simple jump aktivatorius  
    void JumpActivate()
    {
        Jump = true; 
    }

    // double jump aktivatorius 
    void JumpConditionMethod()
    {
        JumpCondition = true;
    }

    ////metodas, kuris apsuka objektus visualiai
    //void Flip()
    //{
    //    facingRight = !facingRight;
    //    Vector3 theScale = transform.localScale;
    //    theScale.x *= -1;
    //    transform.localScale = theScale;
    //}

	//Nužudymas: įprastas 
	void KillPlayer()
	{
		alive = false;

		AudioSource.PlayClipAtPoint(shit, new Vector3(rb2d.position.x, rb2d.position.y, 0));

		anim.SetTrigger("Die");

		// jeigu miršta timescale grįžta į default'ą; 
		Time.timeScale = 1; 

		Invoke("ChangeLevel", 1.0f);
	}

    //Nužudymas: žmogeliuką nužudo "Objektas" 
    void KillPlayer2()
    {
        alive = false;

        AudioSource.PlayClipAtPoint(shit, new Vector3(rb2d.position.x, rb2d.position.y, 0));

        //_SpinAnimation.Spin(); 
        anim.SetTrigger("Spin"); 

        // jeigu miršta timescale grįžta į default'ą; 
        Time.timeScale = 1;

        rb2d.velocity = new Vector2(0, 0);

        rb2d.isKinematic = true; 

        Invoke("ChangeLevel", 1.0f);
    }

    // Nužudymas: išgeria žalią skystį 
    void KillPlayer3()
    {
 
        AudioSource.PlayClipAtPoint(shit, new Vector3(rb2d.position.x, rb2d.position.y, 0));

        // jeigu miršta timescale grįžta į default'ą; 
        Time.timeScale = 1;

		var child2 = gameObject.transform.Find("Weapon").gameObject;
        child2.SetActive(false);

		var child1 = gameObject.transform.Find("Bottle").gameObject;
        child1.SetActive(true);

        // animacija priklausanti nuo judėjimo krypties 
        if (facingRight)
        {
            alive = false;
            Invoke("Fall_left", 0.5f);
        }
        else
        {
            alive = false;
            Invoke("Fall_right", 0.5f);
        }

        Invoke("ChangeLevel", 1.0f);
    }

	void Portal_Escape()
	{
		// gauni buteliukų už tai, kad tiek lakstei 
		bottleCounter = 20; 

		// perkelia žmogeliuką į mapo pradžią 
		rb2d.transform.position = new Vector3 (0.0f, 30.0f, 0.0f); 

		// įjungi normalias kameras 
		camera.enabled = true; 
		camera2.enabled = false; 
		camera3.enabled = false; 
		camera4.enabled = false; 



		// išjungi junginėjimąsi kamerų 
		portal_escape = true; 

		//priešas nebeateina taip greit 
		anotherscript.velocity = new Vector2 (1F, 1F); 
	}

	void Interdimensional_Enemy_Awake() 
	{
		anotherscript.gameObject.SetActive (true); 
	}

	void Interdimensional_Enemy_Show_Up()
	{

		audiosource.Stop (); 
		// audiosource.clip = scaryMusic; 
		// audiosource.volume = GUI.HorizontalSlider(new Rect(25, 25, 200, 60), 0.5f, 0.0f, 1.0f); 
		// audiosource.Play(); 
		Camera.main.fieldOfView = 150.0F; 

		groundVirgin = false; 

		portal_escape = false; 

		Invoke ("SecondCamera", 0.0f);
		Invoke ("ThirdCamera", 0.5f);
		Invoke ("FourthCamera", 1.0f);
		Invoke ("MainCamera", 1.5f);
		Invoke ("Run", 2.0f); 

	}

	// kameros vaizdo efektai artėjant priešui 
	void Interdimensional_Enemy_Camera_Effects() 
	{
		// vaizdo efektai kai priešo range netolimas 
		if ((range < RANGE2) && (portal_escape == false)) { 
			camera.fieldOfView = 120F; 
			camera.enabled = true; 
			camera2.enabled = false; 
			Invoke ("MainCameraNormal", 1.0F); 
			// laikome isjungtą transition 
			transition = false; 
		} else {
			transition = true; 
		}

		// vaizdo efektai kai priešo range tolimesnis (junginėjasi kameros) 
		if (( range < RANGE1)&&(range>=RANGE2)&&(portal_escape == false)&&(transition == true)) { 
			camera.enabled = !camera.enabled; 
			camera2.enabled = !camera2.enabled; 
		}
	}

	void Canvas_Controls()
	{
		scoreText.text = bottleCounter.ToString(); 
		if (groundVirgin == false)
		{
			rangeText.text = (range).ToString (); 
		// int time = (int); 
			timer.text = ""; 
		}
		else {
			timeLeft -= Time.deltaTime; 
			timer.text = ((int)timeLeft).ToString (); 
		}
	}

	void AntiElectricityPowerOff()
	{
		// restartinam 
		shotOfGreen = false; 

		// nebeleidžiame čia įeiti 
		antiLightning = false; 

		StopElectricityEffect (); 

		NotImmortal_2(); 
	}

	void NotImmortal_2 ()
	{
		immortal_2 = false; 
	}

	void ImmortalMode(GameObject weapon, GameObject gun, GameObject bottle) 
	{

		bottleCounter-=10; 

		weapon.SetActive (false);
		gun.SetActive (false);
		bottle.SetActive (true);

		Invoke ("StopDrink", 0.1F); 

		// aktyvuojamas chemikalų efektas 
		var child = gameObject.transform.Find("Hero_1").transform.Find("Chemicals").gameObject; 
		child.SetActive(true);

		Invoke ("StopChemicalsEffect", 3F);

		immortal = true; 
		Invoke ("NotImmortal", 3.5F); 

	}

	void SuperJump(GameObject weapon, GameObject gun, GameObject bottle) 
	{
		weapon.SetActive(false);
		gun.SetActive(false);
		bottle.SetActive(true);

		Invoke ("StopDrink", 0.1F); 

		//if (bottleCounter > 0) {
		//	liquidCounter++; 
		bottleCounter--; 
		//}

		if (bottleCounter > 0) { 
			AudioSource.PlayClipAtPoint (jumpintheair, new Vector3 (rb2d.position.x, rb2d.position.y, 0));
			SPECIALJUMP ();
		}
	}

	// ++++++++++++++++++++++++++
	// +++++++++INVOK'AI+++++++++
	// ++++++++++++++++++++++++++ 

    void Fall_left()
    {
        anim.SetTrigger("Fall_left");
    }

    void Fall_right()
    {
        anim.SetTrigger("Fall_right");
    }

	void WeaponColliderTurnOff()
	{
		ColliderIsOff = true; 
	}

	void StopDrink () 
	{

		var bottle = gameObject.transform.Find("Bottle").gameObject;
		bottle.SetActive(false);

		var weapon = gameObject.transform.Find("Weapon").gameObject;
		weapon.SetActive(true); 

	}

	void StopChemicalsEffect ()
	{
		var child_1 = gameObject.transform.Find("Hero_1").transform.Find("Chemicals").gameObject; 
		child_1.SetActive(false);
	}

	void StopElectricityEffect ()
	{
		var child_1 = gameObject.transform.Find("Hero_1").transform.Find("Electricity").gameObject; 
		child_1.SetActive(false);
	}

	void NotImmortal ()
	{
		immortal = false; 
	}

    void ChangeLevel()
	{
		SceneManager.LoadScene("14"); 
	}

	void MainCamera()
	{
		// Camera.main.enabled = true; 

		camera.enabled = true;
		camera2.enabled = false; 
		camera3.enabled = false;
		camera4.enabled = false; 

	}

	void SecondCamera()
	{
		// Camera.main.enabled = true; 

		camera.enabled = false;
		camera2.enabled = true; 
		camera3.enabled = false; 
		camera4.enabled = false; 

	}

	void ThirdCamera()
	{
		// Camera.main.enabled = true; 

		camera.enabled = false;
		camera2.enabled = false; 
		camera3.enabled = true; 
		camera4.enabled = false; 

	}

	void FourthCamera()
	{
		// Camera.main.enabled = true; 

		camera.enabled = false;
		camera2.enabled = false; 
		camera3.enabled = false; 
		camera4.enabled = true; 

	}

	void MainCameraNormal()
	{
		Camera.main.fieldOfView = state; 
	}

	void Run()
	{
		run.text = "RUN!".ToString(); 

		Time.timeScale = 0.1F; 

		AudioSource.PlayClipAtPoint (runSoundEffect, new Vector3 (rb2d.position.x, rb2d.position.y, 0));

		Invoke ("RunTextTurnOff", 0.25F); 

	}

	void RunTextTurnOff()
	{
		run.text = ""; 

		Time.timeScale = 1F; 
	}

}
