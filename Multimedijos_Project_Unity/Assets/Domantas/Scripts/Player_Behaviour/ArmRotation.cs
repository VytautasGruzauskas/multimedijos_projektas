﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRotation : MonoBehaviour {

    public int rotationOffset = 90;

    private float x; private Vector3 ls;

    private int changer = 90; 

    void Start()
    {
        x = transform.localScale.x;
        ls = transform.localScale;
    }

    void Update()
    {
        Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);

        float rotZ = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        if (dir.x >= 90)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, rotZ - 90);

            ls.x = x;
            transform.localScale = ls;
        }
        else
        {
            transform.rotation = Quaternion.Euler(0f, 0f, rotZ - 90);
            ls.x = -x;
            transform.localScale = ls;
        }
    }

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public float h;

    void FixedUpdate()
    {
        Move();
    }

    void Move()
    {

        // ašis 
        h = Input.GetAxis("Horizontal");

        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();
    }


    //metodas, kuris apsuka objektus visualiai
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        changer = -changer; 
    }
}
