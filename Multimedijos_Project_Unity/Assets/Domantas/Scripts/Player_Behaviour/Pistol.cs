﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : MonoBehaviour {

    public float fireRate = 0;
    public float Damage = 10; 
    // we can choose what LAYERS no to hit 
    public LayerMask notToHit;
	private int bottleCounter; 

	public Transform BulletTrailPrefab; 

    float timeToFire = 0;
    Transform firePoint;

    // Use this for initialization
    void Start () {
		// prisakindina, kas tas firePoint 
        firePoint = transform.Find ("FirePoint"); 
        // if it didn't find the firepoint 
        if (firePoint == null)
        {
            Debug.LogError("No firePoint? "); 
        }
	}
	
	// Update is called once per frame
	void Update () {

		//bottleCounter = this.transform.parent.GetComponent<SimplePlatformController>().bottleCounter; 
		SimplePlatformController script = this.GetComponentInParent<SimplePlatformController>(); 
		bottleCounter = script.bottleCounter; 
		if (fireRate == 0) {
				if (Input.GetMouseButtonDown (1)) {
					if (bottleCounter > 0) {
						Shoot (); 
						script.bottleCounter--; 
					}
				}
		}

        //else
        //{
		//	if(Input.GetMouseButton(1) && Time.time > timeToFire)
        //    {
        //        timeToFire = Time.time + 1/fireRate;
        //        Shoot(); 
        //    }
        //}
	}

    /// <summary>
    /// šaudymas 
    /// </summary>


    void Shoot()
    {
		// updeitinta versija valdymo su pelyte 
		//Vector2 mousePosition = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
		// turi buti leistas buti keiciamas !!! 
        //Vector2 firePointPosition = new Vector2(firePoint.position.x, firePoint.position.y);
		//RaycastHit2D hit = Physics2D.Raycast(firePointPosition, mousePosition - firePointPosition, 1, notToHit);
		Effect (); 
		//Debug.DrawLine((-mousePosition+firePointPosition)*10, firePointPosition, Color.green);
		//Debug.DrawLine(firePointPosition, mousePosition-firePointPosition, Color.green);
        //if (hit.collider != null)
        //{
        //    Debug.DrawLine(firePointPosition, hit.point, Color.red); 
        //}
    }

	void Effect()
	{
		Instantiate (BulletTrailPrefab, firePoint.position, firePoint.rotation); 
	}

}
