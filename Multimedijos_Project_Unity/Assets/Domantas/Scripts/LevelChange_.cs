﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Lygio keitimo skript'as pritaikytas 2D lygiui 

public class LevelChange_ : MonoBehaviour
{
    public GameObject playerObject;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("kita Scena ID: " + PlayerPrefs.GetInt("Next_Scene"));
            SceneManager.LoadScene("Loading_Scene");
        }
    }
}
