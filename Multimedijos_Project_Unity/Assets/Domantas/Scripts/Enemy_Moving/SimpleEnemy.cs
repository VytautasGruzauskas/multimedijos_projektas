﻿using UnityEngine;
using System.Collections;

// Priešo judėjimo algoritmas 
public class SimpleEnemy : MonoBehaviour {

    //Platform options
    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;

    // moving speed of the enemy 
    public float velocity = 3f;

    public Transform sightStart;
    public Transform sightEnd;

    //Platform options
    public float jumpForce = 1000f;

    //Platform options
    public Transform groundCheck;

    //Platform options
//    private bool grounded = false;
    private Animator anim;
    private Rigidbody2D rb2d;

    //Enemy options
    public LayerMask detectWhat;

    //Enemy options
    public bool colliding;

    public int numberofchild;

	[HideInInspector] Vector3 theScale; 

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    //Susikuriame zymekli kuris liecia dalykus ir jei jie uzfiksuoti kaip "Pazinimo sodo vaisiai"
    //tuomet draudzia tai pasiekti ir priesas apsisuka. 
	void Update () {         
        GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, GetComponent<Rigidbody2D>().velocity.y);

        // jeigu neturi head sustoja 
        //if (transform.childCount == numberofchild) 
        if (transform.Find("Head")) // tikrina ar turi child head 
        {

            colliding = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);

            if (colliding)
            {
                //blogas apvertimas
                //transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y * -1);
                velocity *= -1;
                Flip();

                //ikelt is spcontroller
                //grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
                // jump = true;
            }
        }
        else
        {
            // sustoja 
            velocity = 0;
            this.tag = "ground"; 
            //kazko neveikia, galima krentancias grindinis daryt nu kazka idomaus
            //rb2d.isKinematic = false;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        Gizmos.DrawLine(sightStart.position, sightEnd.position);
    }

    void FixedUpdate()
    {
        if (jump)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));

            jump = true;
        }
    }

	// Enemy reakcija į triggerius 
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy_lightning")
        {
            // aktyvuoja elektra 
            // var child = gameObject.transform.GetChild(0).transform.GetChild(0).gameObject;
			var child = gameObject.transform.Find("Body").transform.Find("Electricity").gameObject; 
            child.SetActive(true); 
            velocity = 0; 
            Invoke("BackToNormal", 3); 
        }

		// jei kitas objektas - skystis 
		if (other.gameObject.tag == "Liquid") {
			velocity = 0; 
			// aktyvuoja particles 
			var child = gameObject.transform.Find("Body").transform.Find("Chemicals").gameObject; 
			child.SetActive(true);
			Debug.Log ("Pataikė"); 
			//rb2d.AddForce(new Vector2(0f, jumpForce)); 
			Invoke("BackToNormal", 3);
		}
    }

	void BackToNormal () 
	{

		if (facingRight == false)
			velocity = -3;
		else
			velocity = 3; 

		//išjungiame special effects 
		var child_1 = gameObject.transform.Find("Body").transform.Find("Chemicals").gameObject; 
		child_1.SetActive(false);
		var child_2 = gameObject.transform.Find("Body").transform.Find("Electricity").gameObject; 
		child_2.SetActive(false);
	}

    //metodas, kuris apsuka objektus visualiai
    void Flip()
    {
        facingRight = !facingRight;
        theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
