﻿using UnityEngine;
using System.Collections;

// Visi galimi priešo sunaikinimai 
public class DestroyEnemy : MonoBehaviour
{

    public float destroyDelay = 1f;

    protected bool destroyed = false;

    private Rigidbody2D rb2d;

    private Sprite spriteRenderer;

    public GameObject encodePanel;

    public GameObject go;
    //    Transform t;

    bool alive = true; 

    Renderer[] listOfChildren;

    //vaizdo ir garso laukai
    public AudioClip enemy_death;
    public AudioClip electricity_sound;

    //animatorius
    private Animator anim;

    void Start()
    {
        go = new GameObject();
//        t = go.transform;
        Destroy(go, 2f);
    }

    // Update is called once per frame
    void Update()
    {
    }

    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    bool otherColliderFound;


    // OnTriggerEnter2D kazkaip geriau veikia situo atveju 
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (alive)
        {
            if (other.gameObject.tag == "Weapon")
            {

                // aktyvuoja elektra 
                var child = gameObject.transform.GetChild(0).transform.GetChild(0).gameObject;
                child.SetActive(true);

                //transform.gameObject.SetActive(true); 

                // pakeitus tagą priešas nebegali nužudyti žmogeliuko 
                gameObject.tag = "ground";

                AudioSource.PlayClipAtPoint(electricity_sound, new Vector3(rb2d.position.x, rb2d.position.y, 0));

                AudioSource.PlayClipAtPoint(enemy_death, new Vector3(rb2d.position.x, rb2d.position.y, 0));

                alive = false;

                //nuzudymo animacija 
                GetComponent<Animation>().Play();
                Invoke("Destroying", 0.5f);

            }

            if (other.gameObject.tag == "Player")
            {
                //if (transform.childCount == numberofchild - 1)
                //gameObject.transform.GetChild(3).gameObject.
                //transform.Find("Head") 
                //{
                //    gameObject.tag = "ground";
                //}
            }
        }
    }

    void Destroying()
    {
        Destroy(gameObject);
    }
}
