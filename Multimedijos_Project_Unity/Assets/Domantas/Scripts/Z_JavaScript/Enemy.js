﻿#pragma strict

private var fall : boolean;
var Player : GameObject;
var spawnPoint : Transform;
var stomp : boolean;

function Update () {

}

//Susikuriame zymekli kuris liecia dalykus ir jei jie uzfiksuoti kaip "Pazinimo sodo vaisiai"
//tuomet draudzia tai pasiekti ir priesas apsisuka. 
function OnTriggerEnter(other : Collider)
{
    if(!stomp)
    {
        if (other.tag == "Player")
        {
            Destroy(other.gameObject);
            var P : GameObject = Instantiate(Player, spawnPoint.position, Quaternion.identity);
            var sf = Camera.main.GetComponent(SmoothFollow2);
            sf.target = P.transform;
        } 
    }
}