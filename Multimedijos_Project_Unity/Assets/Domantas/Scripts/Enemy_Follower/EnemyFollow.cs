﻿using UnityEngine;
using System.Collections;

public class EnemyFollow : MonoBehaviour 
{
    [HideInInspector]
    public bool facingRight = true;
    [HideInInspector]
    public bool jump = false;

    [SerializeField]
     private GameObject player;
     private Rigidbody2D body;
	[HideInInspector] public Vector2 velocity;

    //Enemy options
    public bool colliding;

    //detektorius
    public Transform sightStart;
    public Transform sightEnd;
    
    //Enemy options
    public LayerMask detectWhat;

    public float jumpForce = 1000f;
    private Animator anim;
    private Rigidbody2D rb2d;

    public Transform groundCheck;
    private bool grounded = false;

     // Use this for initialization
     void Start()
     {
        body = GetComponent<Rigidbody2D>();

		velocity = new Vector2 (30F, 30F); 
     }

     void Update()
     {
         colliding = Physics2D.Linecast(sightStart.position, sightEnd.position, detectWhat);
         grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

         if (colliding && grounded)
         {
                jump = true;
         }
     }


     // Update is called once per frame
     void FixedUpdate()
     {
         float kampas1 = Mathf.Atan2((player.transform.position.y - body.transform.position.y), (player.transform.position.x - body.transform.position.x));
         float kampas2 = Mathf.Atan2((player.transform.position.x - body.transform.position.x), (player.transform.position.y - body.transform.position.y));
         //Vaiduoklio algoritmas
         if ((player.transform.position.x > body.transform.position.x) && (player.transform.position.y > body.transform.position.y))
         {
             body.velocity = new Vector2(velocity.x * kampas2, velocity.y * kampas1);
         }
         else if ((player.transform.position.x < body.transform.position.x) && (player.transform.position.y > body.transform.position.y))
         {
             //body.velocity = velocity * -1;
             body.velocity = new Vector2(velocity.x * kampas2, velocity.y * kampas1);
         }
         else if ((player.transform.position.x > body.transform.position.x) && (player.transform.position.y < body.transform.position.y))
         {
             body.velocity = new Vector2(velocity.x * kampas2, velocity.y * kampas1);
         }
         else if ((player.transform.position.x < body.transform.position.x) && (player.transform.position.y < body.transform.position.y))
         {
             body.velocity = new Vector2(velocity.x * kampas2, velocity.y * kampas1);
         }
         //else if ((player.transform.position.x < body.transform.position.x) && (player.transform.position.y == body.transform.position.y))
         //{
         //    body.velocity = new Vector2(0, velocity.y * -1);
         //}
         //else if ((player.transform.position.x == body.transform.position.x) && (player.transform.position.y < body.transform.position.y))
         //{
         //    body.velocity = new Vector2(velocity.x * -1, 0);
         //}
         //else if ((player.transform.position.x > body.transform.position.x) && (player.transform.position.y == body.transform.position.y))
         //{
         //    body.velocity = new Vector2(0, velocity.y );
         //}
         //else if ((player.transform.position.x == body.transform.position.x) && (player.transform.position.y > body.transform.position.y))
         //{
         //    body.velocity = new Vector2(velocity.x, 0);
         //}
         else 
         {
             body.velocity = new Vector2(0, 0);
         }

         float h = Input.GetAxis("Horizontal");

         if (h > 0 && !facingRight)
             Flip();
         else if (h < 0 && facingRight)
             Flip();

         if (jump)
         {
             //anim.SetTrigger("Jump");
             //rb2d.AddForce(new Vector2(0f, jumpForce));
             //jump = true;
         }
     }

    //metodas, kuris apsuka objektus visualiai
     void Flip()
     {
         facingRight = !facingRight;
         Vector3 theScale = transform.localScale;
         theScale.x *= -1;
         transform.localScale = theScale;
     }

}