﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

// veikėjo mirties trigeris (ant priešo)  
public class DeathTrigger : MonoBehaviour {

    public AudioClip impact;

    public float delay;

    public GameObject go;

    // pažymime kiek turėtų būti vaikų, kad priešas būtų pavojingas 
    public int numberofchild;

    private Rigidbody2D rb2d;
  
	private Animator anim; 

    void Start()
    {
        go = new GameObject();
        Destroy(go, 2f);
    }

	// Update is called once per frame
	void Update () {
	}

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //kol galva yra tol priesas pavojingas
            //if (t != null)
            //{ 
            // Debug.Log((transform.childCount).ToString() + " = " + numberofchild); 

            var child = gameObject.transform.GetChild(0).transform.GetChild(0).gameObject;

            // jei pamato, kad nesutampa nužudo herojų
			// nenužudo jei neturi head 
			// nenužudo jei jau yra nužudytas (elektra ant jo) 
			if ((transform.childCount == numberofchild) && (child.activeSelf == false))
            {
                //galetu kristi ir issitaskyti che che che
                //print(transform.childCount);

//                KillPlayer();
                //Invoke("ChangeLevel", 2f);
            }
			// jei neturi kaukes - tampa taikiu 
			if (transform.childCount == numberofchild - 1)
				gameObject.tag = "ground"; 
            //}
        }
    }

    //bendras metodas, kas nutinka, kai žmogeliukas/herojus žūsta 
//    void KillPlayer()
//    {
//		Invoke("ChangeLevel", 1.0f);
//    }

	// lygio keitėjas 
//	void ChangeLevel()
//	{
//		//Application.LoadLevel(Application.loadedLevel);
//		SceneManager.LoadScene("14"); 
//	}
}

