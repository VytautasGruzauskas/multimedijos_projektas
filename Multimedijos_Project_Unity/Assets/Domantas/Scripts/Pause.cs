﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {

    public bool pause;

    private float timeScaleBefore;

    // Use this for initialization
    void Start() {
        // pauzė 
        pause = false;
        // iš pradžių greitis lygus vienetui 
        timeScaleBefore = 1;
    }

    // Update is called once per frame
    void Update() {

        //Toggle labai tinka GetKeyDown 
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button7))
        {
            pause = !pause;
            if (pause)
                PauseGame();
            if (!pause)
                ContinueGame();
        }
    }

    // pauzė - activate 
    private void PauseGame()
    {
        Time.timeScale = 0;
        //Disable scripts that still work while timescale is set to 0
    }

    // pauzė - deactivate 
    private void ContinueGame()
    {
        // TimeScaleChanger(); 
        Time.timeScale = timeScaleBefore;
        //enable the scripts again
    }

    // metodas pasunkinantis žaidimą - po kiekvieno pauzės mygtuko paspaudimo - žaidimas pagreitėja 
    private void TimeScaleChanger()
    {
        if (timeScaleBefore < 1.6)
        {
            timeScaleBefore = timeScaleBefore * 11 / 10;
        }
    }

}
