﻿using UnityEngine;
using System.Collections;

public class SpawnManagerAlternate2 : MonoBehaviour {

	[HideInInspector] public static int maxPlatforms = 30;
    public GameObject platform;
    public GameObject platform1;
    public GameObject lightning;
    public GameObject pointer; 
	public GameObject portal;
    public float horizontalMin = 6.5f;
    public float horizontalMax = 14f;
    public float verticalMin = 0f;
    public float verticalMax = 4f;
    int life; 
	public AudioClip sound_of_the_thunder; 

    private Vector2 originPosition;
	Vector2 portalPosition; 

	// Use this for initialization
	void Start () {

        originPosition = transform.position;

        Spawn();

        //Invoke("LIGHTNING", 3.0f);
    }
	
	// Mapo spawninimas
	// Update is called once per frame
	void Spawn () {
	    for (int i = 0; i < maxPlatforms; i++)
        {
            //suranda random atstuma tarp tu
            Vector2 randomPosition = originPosition + new Vector2 (Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax) );
            Vector2 randomPosition2 = randomPosition + new Vector2(Random.Range(-2, 2), 0);
            Instantiate(platform, randomPosition, Quaternion.identity);
            // tikimybė, jog gali objektas ir nepasirodyti 
            if(Random.Range(0, 3) != 1)
                Instantiate(platform1, randomPosition2, Quaternion.identity);
            // taškas nuo kurio tesia 
            originPosition = randomPosition;
			if(maxPlatforms-1 == i)
			{
				Vector2 randomPosition3 = randomPosition + new Vector2(Random.Range(6, 10), 5);
				Instantiate(portal, randomPosition3, Quaternion.identity);
				// portalPosition = randomPosition3; 
			}
		}
        // grąžiname originalią poziciją 
        originPosition = transform.position;
    }

    bool NotCalled = true;
    bool NOTCALLED = true; 

	// periodiškas spawninimas 
    void Update()
    {

        life = 1 + (int) Time.time;
        // Debug.Log(life);

        // kas dešimt sekundžių žaibai 
        if ((life % 10 == 0) && (NotCalled))
        {
            NotCalled = false; 
            LIGHTNING();
        }

        // užtikrina, jog žaibo metodas būtų iškviečiamas vieną kartą 
        if (((life + 1) % 10 == 0))
            NotCalled = true; 

        // perspėjimas apie žaibo vietą 
        if (((life + 3) % 10 == 0) && (NOTCALLED))
        {
            NOTCALLED = false; 
            RODYKLE();
            originPosition = transform.position; 
        }

        // užtikrina, jog žaibo perspėjimo metodas būtų iškviečiamas vieną kartą 
        if (((life + 4) % 10 == 0))
            NOTCALLED = true;

    }

    // masyvai 
    private GameObject[] rodykle = new GameObject[maxPlatforms];
    private GameObject[] zaibas = new GameObject[maxPlatforms];
    Vector2[] randomPosition = new Vector2[maxPlatforms]; 

    void RODYKLE()
    {
        for (int i = 0; i < maxPlatforms; i++)
        {
            randomPosition[i] = originPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), 0);
            // Debug.Log("rnd psc: " + randomPosition[i]); 
			if (i != 0)
            	rodykle[i] = (GameObject) Instantiate(pointer, randomPosition[i], Quaternion.identity);
            originPosition = randomPosition[i];
        }

        for (int i = 0; i < maxPlatforms; i++)
        {
			if (i != 0)
            	Destroy(rodykle[i], 1.5F);
        }
    }

    void LIGHTNING()
    {

        for (int i = 0; i < maxPlatforms; i++)
        {
			if (i != 0)
				AudioSource.PlayClipAtPoint(sound_of_the_thunder, new Vector3(randomPosition[i].x, 100, 0)); 
            //randomPosition = originPosition + new Vector2(3, 0); 
            //randomPosition[i] = originPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), 0);
			if (i != 0)
            	zaibas[i] = (GameObject) Instantiate(lightning, randomPosition[i], Quaternion.identity);
            //originPosition = randomPosition[i];
        }

        for (int i = 0; i < maxPlatforms; i++)
        {
			if (i != 0)
            	Destroy(zaibas[i], 0.1F); 
        }

        // grąžiname pradinę poziciją 
        // originPosition = transform.position; 

        //Invoke("DESTROY", 3.0f);
    }

}
