﻿using UnityEngine;
using System.Collections;

public class SpawnManagerAlternate3 : MonoBehaviour {

    public int maxPlatforms = 20;
    public GameObject platform;
    public GameObject platform1;
    public float horizontalMin = 6.5f;
    public float horizontalMax = 14f;
    public float verticalMin = 0f;
    public float verticalMax = 4f;


    private Vector2 originPosition;

	// Use this for initialization
	void Start () {

        originPosition = transform.position;

        Spawn();
	}
	
	// Update is called once per frame
	void Spawn () {
	    for (int i = 0; i < maxPlatforms; i++)
        {
            //suranda random atstuma tarp tu
            Vector2 randomPosition = originPosition + new Vector2 (Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax) );
            Vector2 randomPosition2 = randomPosition + new Vector2(Random.Range(-2, 2), 0);
            Instantiate(platform, randomPosition, Quaternion.identity);
            // tikimybė, jog gali objektas ir nepasirodyti 
            if(Random.Range(0, 3) != 1)
                Instantiate(platform1, randomPosition2, Quaternion.identity);
            // taškas nuo kurio tesia 
            originPosition = randomPosition;
        }
	}
}
