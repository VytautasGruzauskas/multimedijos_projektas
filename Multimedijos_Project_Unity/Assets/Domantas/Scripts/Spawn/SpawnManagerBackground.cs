﻿using UnityEngine;
using System.Collections;

// almost all the backgrund
public class SpawnManagerBackground : MonoBehaviour {

    public int maxPlatforms = 40;
    public GameObject platform;
	// nuotoliai min/max 
    public float horizontalMin = 6.5f;
    public float horizontalMax = 14f;
	// aukščiai min/max 
    public float verticalMin = 0f;
    public float verticalMax = 4f;
	// reikia dar vieno gylio (neteisingas isdestymas bet nesvarbu :D aplikate i auksti 3D būna ... )
	public float applicateMin = 0;
	public float applicateMax = 14; 
	
	// pasižymime startinę poziciją 
    private Vector3 originPosition;

	// Use this for initialization
	void Start () {

        originPosition = transform.position;

        Spawn();
	}

	void Spawn () {
	    for (int i = 0; i < maxPlatforms; i++)
        {
            //suranda random atstuma tarp tu
			if(Random.Range(0, 3)==0)
			{
				horizontalMax=-horizontalMax; 
				horizontalMin=-horizontalMin; 
			}
            Vector3 randomPosition = originPosition + new Vector3 (Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax), Random.Range(applicateMin, applicateMax));
            Instantiate(platform, randomPosition, Quaternion.identity);
            // taškas nuo kurio tesia 
            originPosition = randomPosition;
			
			//defaultiname horizontales reiksmes 
			horizontalMin = 6.5f;
			horizontalMax = 14f;
        }
	}
}
