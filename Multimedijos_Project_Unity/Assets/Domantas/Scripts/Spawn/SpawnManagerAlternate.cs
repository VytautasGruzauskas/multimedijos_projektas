﻿using UnityEngine;
using System.Collections;

public class SpawnManagerAlternate : MonoBehaviour {

	[HideInInspector] public int maxPlatforms = 20;
    public GameObject platform;
    public GameObject platform1;
    public float horizontalMin = 6.5f;
    public float horizontalMax = 14f;
    public float verticalMin = 0f;
    public float verticalMax = 4f;


    private Vector2 originPosition;

	// Use this for initialization
	void Start () {

        originPosition = transform.position;

        Spawn();
	}
	
	// Update is called once per frame
	void Spawn () {
		Vector2 randomPosition; 
		randomPosition = originPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), Random.Range(15, 20));
		Instantiate(platform, randomPosition, Quaternion.identity);
		Instantiate(platform1, randomPosition, Quaternion.identity);
		for (int i = 1; i < maxPlatforms-6; i++)
        {
            //suranda random atstuma tarp tu

			randomPosition = randomPosition + new Vector2 (Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax) );
            Instantiate(platform, randomPosition, Quaternion.identity);
            Instantiate(platform1, randomPosition, Quaternion.identity);
            originPosition = randomPosition;
            // palengvinimas 
            //verticalMin = verticalMin / 20 * 19;
            //verticalMax = verticalMax / 20 * 19; 
        }
		randomPosition = randomPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), Random.Range(-3, 0));
		Instantiate(platform, randomPosition, Quaternion.identity);
		Instantiate(platform1, randomPosition, Quaternion.identity);
		randomPosition = randomPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), Random.Range(-3, 0));
		Instantiate(platform, randomPosition, Quaternion.identity);
		Instantiate(platform1, randomPosition, Quaternion.identity);
		randomPosition = randomPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), Random.Range(-3, 1));
		Instantiate(platform, randomPosition, Quaternion.identity);
		Instantiate(platform1, randomPosition, Quaternion.identity);
	}
}
