﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// sugeneruojami ivairiaspalviai pastatai 
public class auto_generate_blocks : MonoBehaviour {

	public int maxPlatforms = 10;
	public GameObject object1;
	public GameObject object2;
	public GameObject object3;
	[HideInInspector] int random; 

	private Vector3 originPosition;

	// Use this for initialization
	void Start () {

		//nustatome, jog originali pozicija ten kur tas kubikas stovi 
		originPosition = transform.position;

		Spawn();
	}

	// Update is called once per frame
	void Spawn () {
		for (int i = 0; i < maxPlatforms; i++)
		{
			Vector3 newPosition = originPosition + new Vector3 (0, -10F, 0);
			random = Random.Range (0, 2); 
			Quaternion target = Quaternion.Euler (-90, 0, 180*random); 
			random = Random.Range (0, 2); 
			if (random == 0) 
				Instantiate(object1, newPosition, target);
			else if (random == 1)
				Instantiate(object2, newPosition, target);
			else
				Instantiate(object3, newPosition, target);
			originPosition = newPosition;
		}
	}
}
