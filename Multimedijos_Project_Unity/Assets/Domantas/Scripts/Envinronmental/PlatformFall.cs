﻿using UnityEngine;
using System.Collections;

public class PlatformFall : MonoBehaviour {

    public float fallDelay = 1f;

    private Rigidbody2D rb2d;

    //audio
    //public AudioClip impact;
    //public float fallSoundDelay = 0f;
    //suteikia galimybę paleisti kelwta garsu vienu metu (ilgi nurodai pats)
    //public AudioSource[] sounds;
    //AudioSource audio;
    //AudioSource audiowalk;

    /// <summary>
    /// DeathFall
    /// </summary>
    [HideInInspector] public float lastYVelocityDistance;
    [HideInInspector] public float lastYVelocity;

    /// <summary>
    /// DeathFall
    /// </summary>
    public float fallVelocity = 0f;
    public float deathVelocity = 1000f;

    public Transform groundCheck;
    private bool grounded = false;

    //Apdraudimas, kad be reikalo neleistų "AudioCip touched"
    [HideInInspector] public bool nukrito = false;

    //garso klipai 
    public AudioClip touchedground;
    public AudioClip touched;
    public AudioClip walk;

    void Start()
    {
       
    }

	// Use this for initialization
	void Awake () {
	    rb2d = GetComponent<Rigidbody2D>();
        //sounds = GetComponents<AudioSource>();
        //audio = sounds[0];
        //audiowalk = sounds[1];
	}
	
	// Update is called once per frame
    void Update()
    {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("ground"));

        //padaryti kad vaikstant butu garsas
        if (gameObject.CompareTag("Player"))
        {
            //AudioSource.PlayClipAtPoint(walk, new Vector3(rb2d.position.x, rb2d.position.y, 0));
        }
	}

    /// <summary>
    /// Susidūrimo metodas
    /// </summary>
    /// <param name="other"></param>
    void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {       
            Invoke("Fall", fallDelay);
        }
        if (other.gameObject.CompareTag("ground"))
        {
            AudioSource.PlayClipAtPoint(touchedground, new Vector3(rb2d.position.x, rb2d.position.y, 0));
        }
    }

    /// <summary>
    /// Atšokimo metodas
    /// </summary>
    void Bouncing()
    {
        if (!grounded)
        {
            lastYVelocityDistance = rb2d.velocity.y - lastYVelocity;
            fallVelocity += lastYVelocityDistance < 0 ? lastYVelocityDistance : 0;
        }
        else
        {
            if ((fallVelocity <= -deathVelocity) && (fallVelocity != 0))
            {
                rb2d.AddForce(new Vector2(0f, -lastYVelocity * 15));
            }

            fallVelocity = 0;
        }
        lastYVelocity = rb2d.velocity.y;

    }

    /// <summary>
    /// Kritimo metodas 
    /// </summary>
    void Fall()
    {
        //suskamba garsas
        if(!nukrito)
            AudioSource.PlayClipAtPoint(touched, new Vector3(rb2d.position.x, rb2d.position.y, 0));
        //nukrenta platoforma
        rb2d.isKinematic = false;
        //nebeleidžiame daugiau suskambėti garsui
        nukrito = true;
    }
}
