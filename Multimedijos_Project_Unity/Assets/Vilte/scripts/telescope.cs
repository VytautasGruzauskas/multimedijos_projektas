﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class telescope : MonoBehaviour {
    public Camera zaidejoCamera;
    public Camera[] puzzleCameros;
    public float reach;
    public LayerMask interacibleLayers;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller;
    public GameObject help_tekstas;
    private bool ziuri=false;
    private int kelinta_kamera=0;
    private int dabar_ijungta = 0;
    

	// Use this for initialization
	void Start () {
        for (int i = 0; i < puzzleCameros.Length; i++)
        {
            puzzleCameros[i].gameObject.SetActive(false);
        }
        help_tekstas.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown((KeyCode.E)))
        {
            if (ziuri)
            {
                help_tekstas.SetActive(false);
                zaidejoCamera.gameObject.SetActive(true);
                controller.enabled = true;
                ziuri = false;
                puzzleCameros[dabar_ijungta].gameObject.SetActive(false);
            }
            else {
                Ray ray = new Ray(zaidejoCamera.transform.position, zaidejoCamera.transform.forward);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, reach, interacibleLayers))
                {
                    if (hit.collider.tag == "teleskopas")
                    {
                        help_tekstas.SetActive(true);
                        zaidejoCamera.gameObject.SetActive(false);
                        controller.enabled = false;
                        ziuri = true;
                        puzzleCameros[kelinta_kamera].gameObject.SetActive(true);
                        dabar_ijungta = kelinta_kamera;
                        kelinta_kamera++;
                        if (kelinta_kamera >= puzzleCameros.Length)
                            kelinta_kamera = 0;


                    }

                }
            }
            

        }
		
	}
}
