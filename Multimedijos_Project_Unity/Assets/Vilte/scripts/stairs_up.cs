﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stairs_up : MonoBehaviour {

    float reiksme = 0;
    public float speed = 1;
    bool paspaude = false;
    //Vector3 pradinis;
    public GameObject objektas;
    Vector3 objektoPradinis;
    // Use this for initialization
    void Start () {
        //pradinis = GetComponent<Transform>().position;
        objektoPradinis = objektas.GetComponent<Transform>().position;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Z))
            paspaude = true;
        

        if (paspaude)
        {
           
            reiksme -= speed * Time.deltaTime;
            //GetComponent<Transform>().position = new Vector3(pradinis.x, Mathf.Clamp(reiksme, -2, 2), pradinis.z);
            objektas.GetComponent<Transform>().position = new Vector3(objektoPradinis.x, Mathf.Clamp(reiksme, -2, 2), objektoPradinis.z);
            if (Mathf.Abs(reiksme+2)< 0.1)
            {
                Debug.Log("suveike");
                paspaude = false;
                //GetComponent<Transform>().position = pradinis;
                objektas.GetComponent<Transform>().position = objektoPradinis;
                reiksme = 0;
            }
        }
	}
}
