﻿//using UnityEngine;
//using UnityEditor;

//[CustomEditor(typeof(CableHolder))]
//public class CustomInspectorCable : Editor
//{


//    public GameObject source;
//    SerializedObject m_Object;

//    void OnEnable()
//    {
//        m_Object = new SerializedObject(target);
//    }

//    public override void OnInspectorGUI()
//    {
//        EditorUtility.SetDirty(target);
//        CableHolder script = (CableHolder)target;

//        EditorGUILayout.LabelField(new GUIContent("Cable Touching Directions"));
//        EditorGUILayout.LabelField(new GUIContent(""));

//        EditorGUILayout.Toggle("Is Front Occupied", script.isFrontPointOccupied);
//        EditorGUILayout.Toggle("Is Back Occupied", script.isBackPointOccupied);
//        EditorGUILayout.Toggle("Is Right Occupied", script.isRightPointOccupied);
//        EditorGUILayout.Toggle("Is Left Occupied", script.isLeftPointOccupied);

//        EditorGUILayout.LabelField(new GUIContent(""));
//        EditorGUILayout.LabelField(new GUIContent("Connected Cable Holders"));
//        EditorGUILayout.LabelField(new GUIContent(""));

//        EditorGUILayout.ObjectField("Connected Front Holder", script.ConnectedFrontHolder, typeof(GameObject), true);
//        EditorGUILayout.ObjectField(new GUIContent("Connected Back Holder"), script.ConnectedBackHolder, typeof(GameObject), true);
//        EditorGUILayout.ObjectField(new GUIContent("Connected Right Holder"), script.ConnectedRightHolder, typeof(GameObject), true);
//        EditorGUILayout.ObjectField(new GUIContent("Connected Left Holder"), script.ConnectedLeftHolder, typeof(GameObject), true);

//        EditorGUILayout.LabelField(new GUIContent(""));
//        EditorGUILayout.LabelField(new GUIContent("Other Configs"));
//        EditorGUILayout.LabelField(new GUIContent(""));

//        SerializedProperty m_IntProperty = m_Object.FindProperty("ElectricitySource");
//        EditorGUILayout.PropertyField(m_IntProperty, new GUIContent("Is Electricity Source"), true);
        
//        SerializedProperty m_IntProperty2 = m_Object.FindProperty("ElectricityDestination");
//        EditorGUILayout.PropertyField(m_IntProperty2, new GUIContent("Is Electricity Destination"), true);
//        //EditorGUILayout.Toggle("Is Electricity Source", m_IntProperty);
//        //EditorGUILayout.Toggle("Is Electricity Destination", script.ElectricityDestination);
//        EditorGUILayout.Toggle("Is Electricity Running", script.ElectricityRunning);

//        EditorGUILayout.LabelField(new GUIContent(""));
//        EditorGUILayout.LabelField(new GUIContent("Debug Configs"));
//        EditorGUILayout.LabelField(new GUIContent(""));

//        //EditorGUILayout.Toggle("Debug", script.enableDebug);

//        //SerializedProperty m_IntProperty5 = m_Object.FindProperty("colliders");
//        //EditorGUILayout.PropertyField(m_IntProperty5, new GUIContent("Box Colliders For Gizmos"), true);

//        /*
//        SerializedProperty m_IntProperty3 = m_Object.FindProperty("height");
//        EditorGUILayout.PropertyField(m_IntProperty3, new GUIContent("Height"));
//        */



//        m_Object.ApplyModifiedProperties();
//    }
//}
