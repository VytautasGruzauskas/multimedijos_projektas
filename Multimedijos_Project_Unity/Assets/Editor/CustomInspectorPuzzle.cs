﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Puzzle))]
public class CustomInspector : Editor
{
    public GameObject source;
    SerializedObject m_Object;

    void OnEnable()
    {
        m_Object = new SerializedObject(target);
    }

    public override void OnInspectorGUI()
    {
        Puzzle script = (Puzzle)target;

        //cia rasyti kintamus matomus visuose puzzle tipo pasirinkimuose

        script.type = (Puzzle.puzzleTypes)EditorGUILayout.EnumPopup("Puzzle Types", script.type);
        SerializedProperty puzzleTrigger_Property = m_Object.FindProperty("puzzleTrigger");
        EditorGUILayout.PropertyField(puzzleTrigger_Property, new GUIContent("Puzzle Trigger"));
        script.puzzleCompleted = EditorGUILayout.Toggle("Is Puzzle Done", script.puzzleCompleted);
        m_Object.ApplyModifiedProperties();



        //cia viduj if'u aprasomi kiekvieno pasirinkimo atskiri kintamieji.

        if (script.type == Puzzle.puzzleTypes.pressurePlateSimple)
        {
            SerializedProperty puzzleSolver_Property = m_Object.FindProperty("puzzleSolver");
            EditorGUILayout.PropertyField(puzzleSolver_Property, new GUIContent("Puzzle Solver"));

            m_Object.ApplyModifiedProperties();
        }

        if (script.type == Puzzle.puzzleTypes.pressurePlateParticle)
        {
            SerializedProperty puzzleSolver_Property = m_Object.FindProperty("puzzleSolver");
            EditorGUILayout.PropertyField(puzzleSolver_Property, new GUIContent("Puzzle Solver"));
            SerializedProperty light_IntProperty = m_Object.FindProperty("emissiveMaterial");
            SerializedProperty nonlight_IntProperty = m_Object.FindProperty("nonEmissiveMaterial");
            EditorGUILayout.PropertyField(light_IntProperty, new GUIContent("Emissive Material"));
            EditorGUILayout.PropertyField(nonlight_IntProperty, new GUIContent("Non Emissive Material"));
            m_Object.ApplyModifiedProperties();
        }

        if (script.type == Puzzle.puzzleTypes.lightGame)
        { 
            SerializedProperty m_IntProperty = m_Object.FindProperty("lightObjects");
            SerializedProperty m_IntProperty2 = m_Object.FindProperty("switchObject");
            EditorGUILayout.PropertyField(m_IntProperty, new GUIContent("Lights"), true);
            EditorGUILayout.PropertyField(m_IntProperty2, new GUIContent("Switches"), true);
            m_Object.ApplyModifiedProperties();
        }

        if(script.type == Puzzle.puzzleTypes.pressurePlateWeight)
        {
            script.correctWeight = EditorGUILayout.IntField("Correct Weight", script.correctWeight);
            SerializedProperty m_IntProperty = m_Object.FindProperty("CubeArray");
            SerializedProperty light_IntProperty = m_Object.FindProperty("emissiveMaterial");
            SerializedProperty nonlight_IntProperty = m_Object.FindProperty("nonEmissiveMaterial");
            EditorGUILayout.PropertyField(light_IntProperty, new GUIContent("Emissive Material"));
            EditorGUILayout.PropertyField(nonlight_IntProperty, new GUIContent("Non Emissive Material"));
            EditorGUILayout.PropertyField(m_IntProperty, new GUIContent("WeightObjects"), true);
            m_Object.ApplyModifiedProperties();
        }
    }
}
