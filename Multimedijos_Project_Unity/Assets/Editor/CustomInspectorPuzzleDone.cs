﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PuzzleDone))]
public class CustomInspectorPuzzleDone : Editor
{

    public GameObject source;
    SerializedObject m_Object;

    void OnEnable()
    {
        m_Object = new SerializedObject(target);
    }

    public override void OnInspectorGUI()
    {
        PuzzleDone script = (PuzzleDone)target;

        //cia rasyti kintamus matomus visuose puzzle tipo pasirinkimuose

        script.level = (PuzzleDone.CurrentLevel)EditorGUILayout.EnumPopup("Current Level", script.level);
        //script.
        //script.type = (Puzzle.puzzleTypes)EditorGUILayout.EnumPopup("Puzzle Types", script.type);
        //script.puzzleCompleted = EditorGUILayout.Toggle("Is Puzzle Done", script.puzzleCompleted);

        //cia viduj if'u aprasomi kiekvieno pasirinkimo atskiri kintamieji.

        if (script.level == PuzzleDone.CurrentLevel.Level1)
        {
            //script.puzzleSolver = EditorGUILayout.ObjectField(script.puzzleSolver, typeof(Object), true);
            SerializedProperty m_IntProperty = m_Object.FindProperty("puzzleFinished");
            EditorGUILayout.PropertyField(m_IntProperty, new GUIContent("puzzle Finished Object"));

            SerializedProperty m_IntProperty2 = m_Object.FindProperty("smoothtime");
            EditorGUILayout.PropertyField(m_IntProperty2, new GUIContent("Smooth Time"));

            SerializedProperty m_IntProperty3 = m_Object.FindProperty("height");
            EditorGUILayout.PropertyField(m_IntProperty3, new GUIContent("Height"));

            SerializedProperty m_IntProperty4 = m_Object.FindProperty("bottomHeigth");
            EditorGUILayout.PropertyField(m_IntProperty4, new GUIContent("Bottom Heigth"));

            m_Object.ApplyModifiedProperties();
        }

        if (script.level == PuzzleDone.CurrentLevel.Level2)
        {
            SerializedProperty m_IntProperty = m_Object.FindProperty("puzzleFinished");
            EditorGUILayout.PropertyField(m_IntProperty, new GUIContent("puzzle Finished Object"));

            SerializedProperty m_IntProperty2 = m_Object.FindProperty("smoothtime");
            EditorGUILayout.PropertyField(m_IntProperty2, new GUIContent("Smooth Time"));

            SerializedProperty m_IntProperty3 = m_Object.FindProperty("height");
            EditorGUILayout.PropertyField(m_IntProperty3, new GUIContent("Height"));

            SerializedProperty m_IntProperty4 = m_Object.FindProperty("bottomHeigth");
            EditorGUILayout.PropertyField(m_IntProperty4, new GUIContent("Bottom Heigth"));

            m_Object.ApplyModifiedProperties();
        }

        if (script.level == PuzzleDone.CurrentLevel.Level3)
        {
            SerializedProperty m_IntProperty = m_Object.FindProperty("puzzleFinished");
            EditorGUILayout.PropertyField(m_IntProperty, new GUIContent("puzzle Finished Object"));

            SerializedProperty m_IntProperty2 = m_Object.FindProperty("smoothtime");
            EditorGUILayout.PropertyField(m_IntProperty2, new GUIContent("Smooth Time"));
            /*
            script.correctWeight = EditorGUILayout.IntField("Correct Weight", script.correctWeight);
            SerializedProperty m_IntProperty = m_Object.FindProperty("CubeArray");
            SerializedProperty light_IntProperty = m_Object.FindProperty("emissiveMaterial");
            SerializedProperty nonlight_IntProperty = m_Object.FindProperty("nonEmissiveMaterial");
            EditorGUILayout.PropertyField(light_IntProperty, new GUIContent("Emissive Material"));
            EditorGUILayout.PropertyField(nonlight_IntProperty, new GUIContent("Non Emissive Material"));
            EditorGUILayout.PropertyField(m_IntProperty, new GUIContent("WeightObjects"), true);*/

            m_Object.ApplyModifiedProperties();
        }

        if (script.level == PuzzleDone.CurrentLevel.Level4)
        {

            m_Object.ApplyModifiedProperties();
        }
    }
}
